<?php

namespace App\Transformers;

use App\Models\Document;
use League\Fractal;

/**
 * Class DocumentTransformer - Responsável por padronizar a serialização de objetos Document
 * @package App\Transformers
 */
class DocumentTransformer extends Fractal\TransformerAbstract
{
    public function transform(Document $item)
    {
        $output = [
            'id' => $item->getId(),
            'originalName' => $item->getOriginalName(),
            'extension' => $item->getExtension(),
            'mimeType' => $item->getMimeType(),
            'path' => $item->getPath(),
            'size' => $item->getSize('MB'),
            'url' => $item->getUrl(),
            'storageDisk' => $item->getStorageDisk(),
            'hash' => $item->getHash(),
            'dateTime' => $item->getDateTime(),
        ];

        if ($item->getExif()) {
            $output['exif'] = $item->getExif();
        }

        if ($item->getExplicitContent()) {
            $output['explicitContent'] = $item->getExplicitContent();
        }

        return $output;
    }
}
