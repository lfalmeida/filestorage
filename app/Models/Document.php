<?php

namespace App\Models;

use App\Libraries\Formatter;
use Carbon\Carbon;
use Psr\Log\InvalidArgumentException;

/**
 * Class Document
 *
 * Classe simples para documentos
 *
 * @method getStorageDisk()
 * @method getMimeType()
 * @method getPath()
 * @method getOriginalName()
 */
class Document
{
    protected $id;
    protected $originalName;
    protected $extension;
    protected $mimeType;
    protected $path;
    protected $url;
    protected $storageDisk;
    protected $hash;
    protected $contents;
    protected $dateTime;
    protected $size;
    protected $exif;
    protected $explicitContent = false;

    /**
     * Document constructor.
     * @param array|null $properties
     * @throws \Exception
     */
    public function __construct(array $properties = null)
    {
        if ($properties) {
            foreach ($properties as $property => $value) {
                $setter = sprintf("set%s", ucfirst($property));
                if (!property_exists($this, $property)) {
                    throw new \InvalidArgumentException(sprintf(trans('messages.error.INVALID_PROP'), $property));
                }
                $this->{$setter}($value);
            }
        }
    }

    public function __call($method, $arguments)
    {
        // verificando se é um setter
        if (strpos($method, "set") === 0) {
            $property = lcfirst(substr($method, 3));
            $this->{$property} = $arguments[0] ?? null;
            return $this;
        }

        if (strpos($method, "get") === 0) {
            $property = lcfirst(substr($method, 3));
            return $this->{$property};
        }
        throw new \InvalidArgumentException('Método não encontrado.');
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return app('url')->to($this->getPath());
    }

    /**
     * @param mixed $value
     * @return Document
     */
    public function setHash($value)
    {
        // recebido o caminho do arquivo, gerar o hash
        if (strpos($value, DIRECTORY_SEPARATOR) !== false) {
            $this->hash = md5_file($value);
            return $this;
        }
        // valor recebido já é o hash
        $this->hash = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'originalName' => $this->getOriginalName(),
            'extension' => $this->getExtension(),
            'mimeType' => $this->getMimeType(),
            'path' => $this->getPath(),
            'size' => $this->getSize(),
            'url' => $this->getUrl(),
            'storageDisk' => $this->getStorageDisk(),
            'hash' => $this->getHash(),
            'dateTime' => $this->getDateTime(),
            'exif' => $this->getExif(),
            'explicitContent' => $this->getExplicitContent()
        ];
    }

    /**
     * @return mixed
     */
    public function getDateTime()
    {
        if (empty($this->dateTime) && isset($this->id)) {
            list($timestamp, ,) = explode('.', $this->id);
            $this->dateTime = Carbon::createFromTimestamp($timestamp)->toIso8601String();
        }
        return $this->dateTime;
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function getSize($format = null)
    {
        if ($format) {
            return (new Formatter())->bytes($this->size);
        }
        return $this->size;
    }
}
