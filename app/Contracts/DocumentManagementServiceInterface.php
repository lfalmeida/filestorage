<?php

namespace App\Contracts;

use App\Models\Document;

/**
 * Interface DocumentManagementServiceInterface
 *
 * <p>Define a estrutura para Services de gerenciamento de <b>Documents</b>.</p>
 *
 * @package App\Contracts
 */
interface DocumentManagementServiceInterface
{
    /**
     * Obtém metadados de um Document
     *
     * @param $relativeFilePath
     * @return Document
     * @throws \App\Exceptions\DocumentNotFoundException
     */
    public function get($relativeFilePath): Document;

    /**
     * Exclui permanentemente um Document do disco e remove os registros de metadados.
     * @param $relativePath
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($relativePath);

    /**
     * Realiza a leitura e retorna o conteúdo de um Document no disco
     *
     * @param Document $doc
     * @return bool|false|string
     */
    public function getContents(Document $doc);

    /**
     * Retorna a url web de um Document
     *
     * @param Document $doc
     * @return string
     */
    public function getUrl(Document $doc);
}
