<?php
/**
 * Created by PhpStorm.
 * User: lfalmeida
 * Date: 6/11/17
 * Time: 6:21 PM
 */

namespace App\Contracts;

interface ImageAnalysisClientInterface
{
    /**
     * Recebe uma url web da imagem e realiza a análise
     * @param $imageWebUrl
     * @return mixed
     */
    public function analyze($imageWebUrl);

    /**
     * verifica se a imagem analisada possui conteúdo explícito.
     * @return bool
     */
    public function hasExplicitContent(): bool;
}
