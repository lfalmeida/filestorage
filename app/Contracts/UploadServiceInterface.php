<?php
namespace App\Contracts;

use App\Models\Document;
use Illuminate\Http\UploadedFile;

/**
 * Class Uploader
 * @package App\Libraries
 */
interface UploadServiceInterface
{
    /**
     * Determina qual será o subdiretório utilizado para armazenar o arquivo
     * @param string $subfolder
     */
    public function setResource($subfolder);

    /**
     * Recebe os dados e determina delega a ação para o método que gerencia um ou para o método
     * que gerencia vários arquivos, dependendo do tipo de dados recebidos.
     *
     * @param mixed $files Dados recebidos no request contendo os arquivos
     * @return Document|array Um objeto Documento ou um array de objetos Documento
     * @throws \Exception Lança uma Exception caso os dados recebidos sejam inválidos
     */
    public function handle($files);

    /**
     * Trata o caso do recebimento de vários arquivos no mesmo Request
     * @param array $files Arquivos enviados no Request
     * @return array Arquivos recebidos e tratados
     */
    public function handleMultipleFiles(array $files);

    /**
     * Realiza o upload de um único arquivo e retorna um objeto Document com as informações
     * sobre o arquivo que foi recebido
     *
     * @param $file UploadedFile Arquivo recebido no request
     * @return Document Dados gerados pelo upload
     * @throws \Exception Uma Exception é lançada caso os dados recebidos sejam inválidos
     */
    public function handleSingleFile($file);

    /**
     * Adiciona na lista de arquivos desta instância um Document representando um arquivo
     * @param $document Document
     */
    public function addFileToList(Document $document);

    /**
     * Retorna a lista de de objetos Document desta instância
     * @return array
     */
    public function getUploadedFilesList();
}
