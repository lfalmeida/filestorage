<?php

namespace App\Contracts;

/**
 * Interface DocumentRepositoryInterface
 *
 * Definição da estrutura para repositórios de Document.
 *
 * @package App\Contracts
 */
interface DocumentRepositoryInterface
{
    /**
     * Salva os dados de um documento
     * @param array $entityData dados que serão persistidos
     * @return string ID do objeto criado ou a string '0', em caso de falha
     */
    public function save(array $entityData): string;

    /**
     * Recebe um array assossiativo mapeando os critérios para a busca de um único
     * objeto
     *
     * @param array $criteria
     * @throws \Exception
     */
    public function find(array $criteria);

    /**
     * Atualiza um objeto
     * objeto
     *
     * @param array $criteria
     * @param array $newData
     * @return
     */
    public function update(array $criteria, array $newData);

    /**
     * Remove permanentemente um registro
     *
     * @param array $criteria
     */
    public function delete(array $criteria);
}
