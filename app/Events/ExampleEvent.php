<?php

namespace App\Events;

/**
 * Class ExampleEvent
 * @package App\Events
 * @codeCoverageIgnore
 */
class ExampleEvent extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
