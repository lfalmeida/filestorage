<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 * @codeCoverageIgnore
 * @ignore
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\StorageLinkCommand',
        'App\Console\Commands\ClearViewCache',
        'App\Console\Commands\ConfigCacheCommand',
        'App\Console\Commands\ConfigClearCommand'
    ];
}
