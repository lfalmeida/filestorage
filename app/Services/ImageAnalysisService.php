<?php

namespace App\Services;

use App\Contracts\DocumentRepositoryInterface;
use App\Contracts\ImageAnalysisClientInterface;
use App\Models\Document;

class ImageAnalysisService
{
    /**
     * @var
     */
    protected $result;

    /**
     * @var Document $document;
     */
    protected $document;

    /**
     * @var ImageAnalysisClientInterface
     */
    private $client;

    /**
     * ImageAnalysisService constructor.
     * @param ImageAnalysisClientInterface $client
     */
    public function __construct(ImageAnalysisClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param Document $document
     * @return $this
     */
    public function analyze(Document $document)
    {
        $this->document = $document;
        $this->result = $this->client->analyze($document->getUrl());
        return $this;
    }

    /**
     * verifica se a imagem analisada possui conteúdo explícito.
     * @return bool
     */
    public function hasExplicitContent(): bool
    {
        $hasExplicitContent = $this->client->hasExplicitContent();

        if ($hasExplicitContent) {
            $this->updateDocumentExplicitContentFlag();
            $this->reportExplicitContent();
        }

        return $hasExplicitContent;
    }

    /**
     * Notifica o cliente a detecção do conteúdo explicito
     */
    public function reportExplicitContent()
    {
        /**
         * Implementar lógica de notificação para conteúdo explícito
         */
        return null;
    }

    /**
     * Cria uma propriedade no documento, indicando que este possui conteúdo explícito
     */
    public function updateDocumentExplicitContentFlag()
    {
        $newData = $this->document->toArray();
        $newData['explicitContent'] = true;

        $documentRepo = app(DocumentRepositoryInterface::class);
        $documentRepo->update(['path' => $this->document->getPath()], $newData);
    }
}
