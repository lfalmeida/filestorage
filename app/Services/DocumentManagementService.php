<?php

namespace App\Services;

use App\Contracts\DocumentManagementServiceInterface;
use App\Contracts\DocumentRepositoryInterface as Repository;
use App\Exceptions\DocumentNotFoundException;
use App\Models\Document;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\FilesystemInterface;

/**
 * Class DocumentManagementService
 * @package App\Libraries
 */
class DocumentManagementService implements DocumentManagementServiceInterface
{
    /**
     *
     * @var Repository
     */
    protected $repository;

    /**
     * FileManager constructor.
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Exclui permanentemente um Document do disco e remove os registros de metadados.
     *
     * @param $relativePath
     * @return \Illuminate\Http\JsonResponse
     * @throws DocumentNotFoundException
     */
    public function delete($relativePath)
    {
        $doc = $this->get($relativePath);

        // remove os metadados
        $result = $this->repository->delete(['path' => $relativePath]);

        if ($result) {
            // deleta o arquivo físico do disco
            Storage::disk($doc->getStorageDisk())->delete($relativePath);
            return $result;
        }
        throw new DocumentNotFoundException(trans('messages.error.NOT_REMOVED'));
    }

    /**
     * Obtém metadados de um Document
     *
     * @param $relativeFilePath
     * @return Document
     * @throws DocumentNotFoundException
     */
    public function get($relativeFilePath): Document
    {
        $document = $this->repository->find(['path' => $relativeFilePath]);
        if ($document) {
            return $document;
        }
        throw new DocumentNotFoundException();
    }

    /**
     * Retorna a url web de um Document
     *
     * @param Document $doc
     * @return string
     * @throws Exception
     */
    public function getUrl(Document $doc): string
    {
        switch ($doc->getStorageDisk()) {
            case 'local':
                $url = App::make('url')->to($doc->getPath());
                break;
            case 's3':
                $baseUrlS3 = 'https://s3.amazonaws.com';
                $url = sprintf('%s/%s/%s', $baseUrlS3, env('AWS_BUCKET'), $doc->getPath());
                break;
            default:
                // No momento, discos suportados são 'local' ou 's3'
                $message = 'Provedor de armazenamento não suportado: ' . $doc->getStorageDisk();
                throw new \InvalidArgumentException($message);
        }
        return $url;
    }

    /**
     * @param array $criteria
     * @param array $newData
     */
    public function update(array $criteria, array $newData)
    {
        return $this->repository->update($criteria, $newData);
    }

    /**
     * @param $imageWebUrl
     * @return null|string
     * @throws Exception
     */
    public function getEcodedContentsFromUrl($imageWebUrl)
    {
        $path = implode('/', array_slice(explode('/', $imageWebUrl), -2));
        $document = $this->repository->find(['path' => $path]);
        if ($document) {
            $contents = $this->getContents($document);
            return $contents ? base64_encode($contents) : null;
        }
        return null;
    }

    /**
     * Realiza a leitura e retorna o conteúdo de um Document no disco
     * @param $doc
     * @return bool|false|string
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function getContents(Document $doc)
    {
        $disk = $this->getDiskDriver($doc->getStorageDisk());
        return $disk->read($doc->getPath());
    }

    /**
     * @param $storageDisk
     * @return FilesystemInterface
     */
    private function getDiskDriver($storageDisk): FilesystemInterface
    {
        return Storage::disk($storageDisk)->getDriver();
    }
}
