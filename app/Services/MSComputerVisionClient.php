<?php

namespace App\Services;

use App\Contracts\ImageAnalysisClientInterface;
use GuzzleHttp\Client as HttpClient;

/**
 * Class MSComputerVisionClient
 * @package App\Services
 */
class MSComputerVisionClient implements ImageAnalysisClientInterface
{
    /**
     * @var HttpClient Instância do cliente Http
     */
    protected $client;

    /**
     * @var array
     */
    protected $apiInfo = [
        'baseUrl' => 'https://westcentralus.api.cognitive.microsoft.com/vision',
        'version' => 'v1.0',
    ];

    /**
     * Armazena o resultado da análise recebida através da consulta a API
     * @var
     */
    protected $result;

    /**
     * MicrosoftComputerVisionService constructor.
     */
    public function __construct()
    {
        // Atribuindo uma instância do Cliente Http para este objeto
        $this->client = app(HttpClient::class);
    }

    /**
     * Recebe uma url web da imagem e realiza a análise a api da Microsoft
     * @param $imageWebUrl
     * @return $this|mixed
     */
    public function analyze($imageWebUrl)
    {
        $enpoint = 'analyze';
        $params = ['visualFeatures' => 'Adult'];
        $url = $this->getEndpointUrl($enpoint, $params);

        $response = $this->client->post($url, [
            'json' => ['url' => $imageWebUrl],
            'headers' => [
                'Content-Type' => 'application/json',
                'Ocp-Apim-Subscription-Key' => env('MICROSOFT_VISION_API_KEY', '8e745ab1103e4b85808708b84e894161'),
            ]
        ]);

        $this->result = json_decode($response->getBody());
        return $this;
    }

    /**
     * Constrói a url para consulta, integrando a url base, endpoint e a API Key
     * @param $endpoint string  Nome do Endpoint que será consultado
     * @return string URL pronta para ser utilizada na consulta
     */
    private function getEndpointUrl($endpoint, $params)
    {
        $url = sprintf('%s/%s/%s?', $this->apiInfo['baseUrl'], $this->apiInfo['version'], $endpoint);
        return $url . http_build_query($params);
    }

    /**
     * verifica se a imagem analisada possui conteúdo explícito.
     * @return bool
     * @throws \Exception
     */
    public function hasExplicitContent(): bool
    {
        if (!$this->result) {
            throw new \InvalidArgumentException(sprintf(
                trans('messages.error.BAD_METHOD_CALL_IMAGE_ANALYSIS'),
                __METHOD__
            ));
        }
        return isset($this->result->adult) &&
            $this->result->adult->isAdultContent !== false ||
            $this->result->adult->isRacyContent !== false;
    }
}
