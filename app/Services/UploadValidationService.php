<?php

namespace App\Services;

use App\Exceptions\DocumentTooLargeException;
use App\Exceptions\MimeTypeNotAllowedException;
use App\Exceptions\MimeTypeNotWhitelistedException;
use Illuminate\Http\UploadedFile;

class UploadValidationService
{
    /**
     * @param UploadedFile $uploadedFile
     * @param array $allowedMimeTypes
     * @return bool
     * @throws MimeTypeNotAllowedException
     * @throws MimeTypeNotWhitelistedException
     */
    public function validateMimeType(UploadedFile $uploadedFile, array $allowedMimeTypes)
    {
        $mime = $uploadedFile->getMimeType();

        // Verfica se está na whitelist
        if (!in_array($mime, config('upload.default.whitelist'))) {
            $messageString = trans('messages.error.MIME_TYPE_NOT_WHITELISTED_EXCEPTION');
            $message = sprintf($messageString, $mime);
            throw new MimeTypeNotWhitelistedException($message);
        }

        // Verifica se é do tipo esperado no contexto
        if (!in_array($mime, $allowedMimeTypes)) {
            $messageString = trans('messages.error.INVALID_MIME_TYPE_EXCEPTION');
            $message = sprintf($messageString, $mime, implode(', ', $allowedMimeTypes));
            throw new MimeTypeNotAllowedException($message);
        }

        return true;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return bool
     */
    public function validateImage(UploadedFile $uploadedFile)
    {
        $allowed = config('upload.default.images');
        return $this->validateMimeType($uploadedFile, $allowed);
    }

    /**
     * @param string $resourname
     * @return mixed
     */
    public function getAllowedMimeTypesByResource(string $resourname)
    {
        $key = 'upload.resources.' . $resourname . '.allowedMimeTypes';
        return config($key, null);
    }

    public function validateResourceUpload(UploadedFile $uploadedFile, $resourceName)
    {
        $allowedMimes = $this->getAllowedMimeTypesByResource($resourceName);

        if ($allowedMimes) {
            $this->validateMimeType($uploadedFile, $allowedMimes);
        }

        $maxFileSize = $this->getMaxSizeByResource($resourceName);

        if ($maxFileSize) {
            $this->validateSize($uploadedFile, $maxFileSize);
        }

        return true;
    }

    /**
     * @param $resourceName
     * @return mixed
     */
    private function getMaxSizeByResource($resourceName)
    {
        $key = 'upload.resources.' . $resourceName . '.maxSize';
        return config($key, 0);
    }

    /**
     * @param $uploadedFile
     * @param $maxFileSize
     * @return bool
     * @throws \Exception
     */
    private function validateSize(UploadedFile $uploadedFile, $maxFileSize)
    {
        $sizeInMB = $uploadedFile->getSize() / 1000 / 1000;
        if ($maxFileSize !== 0 && $sizeInMB > $maxFileSize) {
            $messageText = trans('messages.error.INVALID_DOCUMENT_SIZE');
            $errorMessage = sprintf($messageText, number_format($sizeInMB, 2), $maxFileSize);
            throw new DocumentTooLargeException($errorMessage);
        }
        return true;
    }

    /**
     * Verifica se um arquivo de upload está dentro da lista de mime types permitidos no arquivo de configuração.
     *
     * @param UploadedFile $file
     * @return bool
     * @throws MimeTypeNotWhitelistedException
     */
    public function validateWhitelisted(UploadedFile $file)
    {
        $mimeTypesAllowed = config('upload.default.whitelist');
        $whitelist = array_values($mimeTypesAllowed);
        if (!in_array($file->getMimeType(), $whitelist)) {
            $message = trans('messages.error.MIME_TYPE_NOT_WHITELISTED_EXCEPTION');
            throw new MimeTypeNotWhitelistedException(sprintf($message, $file->getMimeType()));
        }
        return true;
    }
}
