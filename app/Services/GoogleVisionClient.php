<?php

namespace App\Services;

use App\Contracts\ImageAnalysisClientInterface;
use App\Exceptions\UnexpectedValueException;
use GuzzleHttp\Client as HttpClient;

/**
 * Class GoogleVisionClient
 * <p> Realiza a análise de imagens através da API Google Vision, com a finalidade de impedir que os usuários
 *  enviem fotos com conteúdo impróprio. </p>
 *
 * @package App\Services
 */
class GoogleVisionClient implements ImageAnalysisClientInterface
{
    /**
     * Instância do cliente Http
     * @var HttpClient Instância do cliente Http GuzzleHttp\Client
     */
    protected $client;

    /**
     * Dados da API Remota
     * @var array
     */
    protected $apiInfo = [
        'baseUrl' => 'https://vision.googleapis.com',
        'version' => 'v1',
    ];

    /**
     * Armazena o resultado da última análise
     * @var
     */
    protected $result;

    /**
     * GoogleVisionClient constructor.
     */
    public function __construct()
    {
        // Atribuindo uma instância do Cliente Http para este objeto
        $this->client = app(HttpClient::class);
    }

    /**
     * Recebe uma url web da imagem e realiza a análise na API remota
     * @param $imageWebUrl
     * @return $this|mixed
     */
    public function analyze($imageWebUrl)
    {
        $enpoint = 'images:annotate';
        $url = $this->getEndpointUrl($enpoint);

        $requestData = $this->buildRequestData($imageWebUrl);

        $response = $this->client->post($url, [
            'json' => $requestData,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);

        $this->result = json_decode($response->getBody());
        return $this;
    }

    /**
     * Constrói a url para consulta, integrando a url base, endpoint e a API Key
     * @param $endpoint string  Nome do Endpoint que será consultado
     * @return string URL pronta para ser utilizada na consulta
     */
    private function getEndpointUrl($endpoint)
    {
        $key = env('GOOGLE_CLOUD_API_KEY');
        return sprintf('%s/%s/%s?key=%s', $this->apiInfo['baseUrl'], $this->apiInfo['version'], $endpoint, $key);
    }

    /**
     * Constrói e retorna um array que define a estrutura do objeto que será enviado na consulta na API Remota
     * @param $imageWebUrl string URL da imagem acessível na web
     * @return array
     * @throws \Exception
     */
    private function buildRequestData($imageWebUrl): array
    {
        $docService = app(DocumentManagementService::class);
        $content = $docService->getEcodedContentsFromUrl($imageWebUrl);

        if (!$content) {
            throw new \InvalidArgumentException(trans('messages.error.FAILED_ENCODING_IMAGE'));
        }

        return [
            'requests' => [
                'image' => [
                    'content' => $content
                ],
                'features' => [
                    [
                        'type' => 'SAFE_SEARCH_DETECTION',
                        'maxResults' => '1'
                    ]
                ]
            ]
        ];
    }

    /**
     * Verifica se a imagem analisada possui conteúdo explícito.
     * @return bool
     * @throws \Exception
     */
    public function hasExplicitContent(): bool
    {
        if (!$this->result) {
            throw new \BadMethodCallException(sprintf(
                trans('messages.error.BAD_METHOD_CALL_IMAGE_ANALYSIS'),
                __METHOD__
            ));
        }

        if (!isset($this->result->responses[0]->safeSearchAnnotation)) {
            \Log::debug(trans('messages.error.UNESPECTED_EXTERNAL_API_RESPONSE'), (array)$this->result);
            throw new UnexpectedValueException(trans('messages.error.UNESPECTED_EXTERNAL_API_RESPONSE'));
        }

        $requestResult = $this->result->responses[0]->safeSearchAnnotation;
        $properties = ['adult', 'spoof', 'medical', 'violence'];

        foreach ($properties as $prop) {
            if (in_array($requestResult->{$prop}, ['POSSIBLE', 'LIKELY', 'VERY_LIKELY'])) {
                return true;
            }
        }
        return false;
    }
}
