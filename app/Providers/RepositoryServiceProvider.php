<?php

namespace App\Providers;

use App\Contracts\DocumentRepositoryInterface;
use App\Repositories\DocumentRepositoryMongo;
use App\Repositories\DocumentRepositoryRedis;
use Illuminate\Support\ServiceProvider;
use MongoDB\Client as MongoClient;
use Predis\Client as RedisClient;

/**
 * Class AppServiceProvider
 * @package App\Providers
 * @codeCoverageIgnore
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // definindo instância MongoClient
        $this->app->bind('MongoClient', function () {
            $username = env('MONGODB_USERNAME', '');
            $password = env('MONGODB_PASSWORD', '');
            $host = env('MONGODB_HOST');
            $port = env('MONGODB_PORT');
            $database = env('MONGODB_DATABASE');
            $uri = sprintf('mongodb://%s:%s@%s:%s/%s', $username, $password, $host, $port, $database);
            return new MongoClient($uri);
        });

        // definindo instância RedisClient
        $this->app->bind('RedisClient', function () {
            return new RedisClient([
                'scheme' => 'tcp',
                'host' => env('REDIS_HOST'),
                'port' => env('REDIS_PORT'),
            ], ['prefix' => 'docs:']);
        });

        // Definindo qual instância concreta será retornada ao solicitar DocumentRepositoryInterface
        $this->app->bind(
            DocumentRepositoryInterface::class,
            DocumentRepositoryMongo::class
        );
    }
}
