<?php

namespace App\Providers;

use App\Contracts\ImageAnalysisClientInterface;
use App\Contracts\ImageAnalysisServiceInterface;
use App\Services\GoogleVisionClient;
use App\Services\ImageAnalysisService;
use App\Services\MSComputerVisionClient;
use Illuminate\Support\ServiceProvider;

class ImageAnalysisServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ImageAnalysisClientInterface::class, function () {
            return new GoogleVisionClient();
        });
        $this->app->bind(ImageAnalysisServiceInterface::class, function () {
            return new ImageAnalysisService(app(ImageAnalysisClientInterface::class));
        });
    }

    public function boot()
    {
    }
}
