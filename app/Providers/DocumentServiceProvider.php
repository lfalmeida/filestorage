<?php

namespace App\Providers;

use App\Contracts\DocumentRepositoryInterface;
use App\Contracts\UploadServiceInterface;
use App\Services\DocumentManagementService;
use App\Services\UploadService;
use App\Services\UploadValidationService;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 * @codeCoverageIgnore
 */
class DocumentServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Registrando uma instância concreta para a interface do upload
        $this->app->bind(UploadServiceInterface::class, function ($app) {
            return new UploadService($app->make(DocumentRepositoryInterface::class));
        });

        // Registrando uma instância concreta para a interface de validação de upload
        $this->app->bind(UploadValidationService::class, function () {
            return new UploadValidationService();
        });

        // Registrando uma instancia concreta para o serviço de gerenciamento de Documents
        $this->app->bind(DocumentManagementService::class, function ($app) {
            return new DocumentManagementService($app->make(DocumentRepositoryInterface::class));
        });
    }
}
