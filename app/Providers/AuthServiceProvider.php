<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

const API_TOKEN_KEY = 'api_token';

/**
 * Class AuthServiceProvider
 * @package App\Providers
 * @codeCoverageIgnore
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input(API_TOKEN_KEY)) {
                return User::where(API_TOKEN_KEY, $request->input(API_TOKEN_KEY))->first();
            }
        });
    }
}
