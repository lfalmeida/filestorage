<?php


/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     basePath="/",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="FileStorage API",
 *         description="API para armazenamento de arquivos.",
 *         @SWG\Contact(
 *             email="lfalmeida@me.com"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Para mais informações, acesse:",
 *         url="http://poclumen.app"
 *     ),
 *     @SWG\Definition(
 *         definition="ErrorModel",
 *         type="object",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */

/**
 * @SWG\Parameter(
 *     name="Authorization",
 *     in="header",
 *     type="string",
 *     required=true
 *  )
 */
