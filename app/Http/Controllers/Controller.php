<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Laravel\Lumen\Routing\Controller as BaseController;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\TransformerAbstract;

/**
 * Class Controller
 * @package App\Http\Controllers
 * @codeCoverageIgnore
 */
class Controller extends BaseController
{
    /**
     * Create the response for an item.
     *
     * @param  mixed $item
     * @param  TransformerAbstract $transformer
     * @param  int $status
     * @param  array $headers
     * @return Response
     */
    protected function buildItemResponse($item, TransformerAbstract $transformer, $status = 200, array $headers = [])
    {
        $resource = new Item($item, $transformer);
        return $this->buildResourceResponse($resource, $status, $headers);
    }

    /**
     * Create the response for a collection.
     *
     * @param  mixed $collection
     * @param  TransformerAbstract $transformer
     * @param  int $status
     * @param  array $headers
     * @return Response
     */
    protected function buildCollectionResponse(
        $collection,
        TransformerAbstract $transformer,
        $status = 200,
        array $headers = []
    ) {
        $resource = new Collection($collection, $transformer);
        return $this->buildResourceResponse($resource, $status, $headers);
    }

    /**
     * Create the response for a resource.
     *
     * @param  ResourceAbstract $resource
     * @param  int $status
     * @param  array $headers
     * @return Response
     */
    protected function buildResourceResponse(ResourceAbstract $resource, $status = 200, array $headers = [])
    {
        $fractal = app('League\Fractal\Manager');
        return response()->json(
            $fractal->createData($resource)->toArray(),
            $status,
            $headers
        );
    }
}
