<?php

namespace App\Http\Controllers;

use App\Exceptions\DocumentNotFoundException;
use App\Exceptions\MimeTypeNotAllowedException;
use App\Exceptions\UnexpectedValueException;
use App\Libraries\Mime;
use App\Models\Document;
use App\Services\DocumentManagementService;
use App\Services\UploadService;
use App\Transformers\DocumentTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

/**
 * Class DocumentsController - Gerencia os Documents
 * @package App\Http\Controllers
 */
class DocumentsController extends Controller
{
    /**
     * Caminho utilizado
     */
    const PLACEHOLDER_URL = '%s/%s';

    /**
     * Instância do Serviço de gerenciamento de Documents
     * @var mixed
     */
    protected $documentService;

    /**
     * Parâmetros que serão suportados para manipulação de imagens. Ex.: "w => 300" redimensiona uma
     * imagem para 300px de largura
     *
     * @var array|mixed
     */
    protected $glideKeys = [];

    /**
     * DocumentsController constructor.
     */
    public function __construct()
    {
        $this->documentService = App::make(DocumentManagementService::class);
        $this->glideKeys = config('glide.keys');
        $this->middleware('simpleApiAuth', [
            'except' => [
                'showDocument',
            ]
        ]);
    }

    /**
     * Realiza o upload de um ou vários arquivos
     *
     * @SWG\Post(
     *     path="/{resource}",
     *     tags={"Document Management"},
     *     operationId="adminLogin",
     *     summary="Upload de arquivos",
     *     description="Recebe um usuario e senha, autentica e retorna um token de acesso.",
     *     consumes={"mulitpart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="uploadFile",
     *         in="formData",
     *         description="Arquivo que será enviado.",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Parameter(
     *         name="resource",
     *         in="path",
     *         description="Nome do resource a qual o arquivo pertence. Ex.: users",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input",
     *     )
     * )
     *
     *
     * @param string $resource
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\Facades\Response
     * @throws \Exception
     */
    public function upload($resource, Request $request)
    {
        $files = $this->extractFilesFromRequest($request);
        /**  @var UploadService $uploadService * */
        $uploadService = App::make(UploadService::class);

        $resourceName = $resource ?? $request->input('resource');

        $uploadService->setResource($resourceName);

        $uploadService->handle($files);
        $filesList = collect($uploadService->getUploadedFilesList());

        if ($filesList->count() === 1) {
            return $this->buildItemResponse($filesList->first(), new DocumentTransformer());
        }

        return $this->buildCollectionResponse($filesList->all(), new DocumentTransformer());
    }

    /**
     * Examina o request em busca de arquivos enviados, os extrai e os retorna para que sejam
     * tratados pelos métodos responsáveis.
     *
     * @param Request $request
     * @return array|UploadedFile|null
     */
    private function extractFilesFromRequest(Request $request)
    {
        // busca por arquivos enviados
        $files = $request->file();
        if ($files) {
            // instância(s) de UploadedFile
            return $files;
        }

        // verifica se foram enviados arquivos codificados com base64
        $requestData = $request->except(['_url', 'key']);
        $base64Files = [];
        if ($requestData) {
            foreach ($requestData as $value) {
                // converte o base64 em UploadedFile
                $uploadedFile = $this->base64ToUploadedFile($value);
                if ($uploadedFile) {
                    $base64Files[] = $uploadedFile;
                }
            }
        }
        // retorna instância(s) de UploadedFile
        return $base64Files;
    }

    /**
     * Verifica se a váriavel recebida é como parâmetro é um base64 válido e converte em uma
     * instância de <b>UploadedFile</b> para que possa ser tratada da mesma forma de um upload tradicional.
     *
     * @param $base64Data
     * @return bool|UploadedFile
     */
    private function base64ToUploadedFile($base64Data)
    {
        if (!is_string($base64Data)) {
            throw new Exception('Os parâmetros informados para upload estão incorretos.');
        }

        $parts = explode(',', $base64Data);

        if (!isset($parts[1]) || base64_encode(base64_decode($parts[1], true)) !== $parts[1]) {
            throw new Exception('Arquivo Base 64 inválido');
        }
        $tmpPath = tempnam(sys_get_temp_dir(), 'Base64Upload');
        $ifp = fopen($tmpPath, 'wb');
        list($base64Header, $base64String) = explode(',', $base64Data);

        if (!empty($base64Header) && !empty($base64String)) {
            $mimeType = str_replace(['data:', ';base64'], ['', ''], $base64Header);
            fwrite($ifp, base64_decode($base64String));
            fclose($ifp);
            $extension = (new Mime())->getExtensionForType($mimeType);
            $generatedName = sprintf('%s.%s', md5($tmpPath), $extension);
            return new UploadedFile($tmpPath, $generatedName, $mimeType);
        }

        return false;
    }

    /**
     * Exibe um arquivo
     *
     * @SWG\Get(
     *     path="/{resource}/{filename}",
     *     tags={"Document Management"},
     *     operationId="showDocument",
     *     summary="Acessar um Documento",
     *     description="Acessa um documento",
     *     produces={"application/octet-stream"},
     *     @SWG\Parameter(
     *         name="resource",
     *         in="path",
     *         description="Nome do resource ao qual o arquivo pertence. Ex.: users",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="filename",
     *         in="path",
     *         description="Nome do documento desejado. Ex.: 1495374867.59219c135c3134.83734291.jpg",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input",
     *     )
     * )
     *
     *
     * @param Request $request
     * @param $resource
     * @param $filename
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Laravel\Lumen\Http\Redirector
     * @throws DocumentNotFoundException
     * @throws MimeTypeNotAllowedException
     */
    public function showDocument(Request $request, $resource, $filename)
    {
        // verifica se foram enviados parâmetros de manipulação do glide
        $params = array_keys($request->except('_url'));
        if (!empty(array_intersect($params, $this->glideKeys))) {
            // direciona para o método de manipulação
            return $this->transformImage($request, $resource, $filename);
        }

        // monta o path para recuperar o Document
        $relativeFilePath = sprintf(self::PLACEHOLDER_URL, $resource, $filename);

        // obtem os metadados do Documento
        /** @var $document Document */
        $document = $this->documentService->get($relativeFilePath);

        // se for armazenado externamente,
        // direcionar para o método especializado em recuperar arquivos remotos
        if ($document->getStorageDisk() != 'local') {
            return $this->showFromExternalDisk($document);
        }
        return $this->showFromLocalDisk($document);
    }

    /**
     * Manipula uma imagem de acordo com os parâmetros fornecidos na query string do request
     *
     * @param Request $request
     * @param $resource
     * @param $filename
     * @throws MimeTypeNotAllowedException
     */
    public function transformImage(Request $request, $resource, $filename)
    {
        $relativeFilePath = sprintf(self::PLACEHOLDER_URL, $resource, $filename);
        /** @var Document $document */
        $document = null;
        $document = $this->documentService->get($relativeFilePath);

        // Validar se o documento é uma imagem
        $this->ensureImageMime($request, $document);

        $server = \League\Glide\ServerFactory::create([
            'source' => Storage::disk($document->getStorageDisk())->getDriver(),
            'cache' => Storage::disk('local')->getDriver(),
            'cache_path_prefix' => '.cache/glide',
        ]);

        $server->outputImage($document->getPath(), $request->all());
    }

    /**
     * Valida se o Document recebido é uma imagem suportada para manipulação, analisando o seu MimeType
     *
     * @param Request $request
     * @param $document
     * @return bool
     * @throws MimeTypeNotAllowedException
     */
    private function ensureImageMime(Request $request, Document $document)
    {
        if (!in_array($document->getMimeType(), config('upload.default.images'))) {
            $params = array_keys($request->except('_url'));
            $imageOnlyParams = array_intersect($params, $this->glideKeys);
            $message = sprintf(trans('messages.error.IMAGES_ONLY_PARAM_EXCEPTION'), implode(', ', $imageOnlyParams));
            throw new MimeTypeNotAllowedException($message);
        }
        return true;
    }

    /**
     * Obter a url do objeto e redirecionar evitando fazer o download do arquivo do
     * serviço externo para o ambiente local para poder retornar o arquivo
     * @param Document $doc
     * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
     */
    private function showFromExternalDisk(Document $doc)
    {
        $url = $this->documentService->getUrl($doc);
        return redirect($url);
    }

    /**
     * Exibe um documento que está armazenado localmente.
     *
     * @param Document $document
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws DocumentNotFoundException
     */
    private function showFromLocalDisk(Document $document)
    {
        // faz a leitura do arquivo no disco, obtendo seu conteúdo
        $contents = $this->documentService->getContents($document);

        if ($contents) {
            return response()->make($contents, 200, [
                'Content-Type' => $document->getMimeType(),
                'Content-Disposition' => sprintf('inline; filename="%s"', $document->getOriginalName())
            ]);
        }

        throw new DocumentNotFoundException(trans('messages.error.DOC_NOT_FOUND_REPO'));
    }

    /**
     * Exibe metadados de um determinado arquivo
     *
     * @SWG\Get(
     *     path="/{resource}/{filename}/meta",
     *     tags={"Document Management"},
     *     operationId="showMetadata",
     *     summary="Metadados de um Documento",
     *     description="Exibe informações sobre o documento solicitado.",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="resource",
     *         in="path",
     *         description="Nome do resource ao qual o arquivo pertence. Ex.: users",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="filename",
     *         in="path",
     *         description="Nome do documento desejado. Ex.: 1495374867.59219c135c3134.83734291.jpg",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input",
     *     )
     * )
     * @param $resource
     * @param $filename
     * @return \Illuminate\Support\Facades\Response
     */
    public function showMetadata($resource, $filename)
    {
        // monta o path para recuperar o Document
        $relativeFilePath = sprintf(self::PLACEHOLDER_URL, $resource, $filename);
        // obtem os metadados do Documento
        $document = $this->documentService->get($relativeFilePath);
        return $this->buildItemResponse($document, new DocumentTransformer());
    }

    /**
     * Remove permamentemente um documento
     *
     * @SWG\Delete(
     *     path="/{resource}/{filename}",
     *     tags={"Document Management"},
     *     operationId="deleteFile",
     *     summary="Excluir um Documento",
     *     description="Exclui permanentemente um Documento",
     *     produces={"application/octet-stream"},
     *     @SWG\Parameter(
     *         name="resource",
     *         in="path",
     *         description="Nome do resource ao qual o arquivo pertence. Ex.: users",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="filename",
     *         in="path",
     *         description="Nome do documento que será excluído. Ex.: 1495374867.59219c135c3134.83734291.jpg",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input",
     *     )
     * )
     *
     *
     * @param $resource
     * @param $filename
     * @return mixed
     * @throws \Exception
     */
    public function deleteFile($resource, $filename): JsonResponse
    {
        $filePath = sprintf(self::PLACEHOLDER_URL, $resource, $filename);

        $result = $this->documentService->delete($filePath);

        if ($result) {
            return response()->json();
        }

        throw new UnexpectedValueException("Não foi possível excluir o documento.");
    }
}
