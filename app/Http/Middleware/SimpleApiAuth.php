<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class ExampleMiddleware
 * @package App\Http\Middleware
 * @codeCoverageIgnore
 */
class SimpleApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headerKey = $request->headers->get('Authorization') ?? null;
        $paramKey = $request->input('key') ?? null;
        $apiKey = $headerKey ?? $paramKey;

        if ($apiKey && trim(str_replace('Bearer ', '', $apiKey)) === env('API_KEY')) {
            return $next($request);
        }
        return response()->json(['error' => 'Not authorized.'], 403);
    }
}
