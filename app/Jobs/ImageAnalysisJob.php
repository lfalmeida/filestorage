<?php

namespace App\Jobs;

use App\Contracts\ImageAnalysisServiceInterface;
use App\Models\Document;
use App\Services\DocumentManagementService;

/**
 * Class ImageAnalysisJob
 * @package App\Jobs
 */
class ImageAnalysisJob extends Job
{
    /**
     * @var Document
     */
    protected $document;

    /**
     * ImageAnalysisJob constructor.
     * @param Document $document
     */
    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        // não executar a análise nos testes unitários e ambiente local
        if (env('APP_ENV') !== 'local' && env('APP_ENV') != 'testing') {
            $service = app(ImageAnalysisServiceInterface::class);
            return $service->analyze($this->document)
                ->hasExplicitContent();
        }
        return true;
    }
}
