<?php

namespace App\Libraries;

use Dotenv\Exception\InvalidFileException;

/**
 * Class Mime
 * @package App\Libraries
 */
class Mime
{
    protected $mimeTypes;

    /**
     * Mime constructor.
     */
    public function __construct()
    {
        $this->loadDataSource();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function loadDataSource()
    {
        $dataSourcePath = resource_path('mimeTypes.json');
        if (!file_exists($dataSourcePath)) {
            throw new InvalidFileException('Mapa de mime-types não encontrado: ' . $dataSourcePath);
        }
        $this->mimeTypes = json_decode(file_get_contents($dataSourcePath), true);
        return !empty($this->mimeTypes);
    }

    /**
     * @return mixed
     */
    public function getTypes()
    {
        return $this->mimeTypes;
    }

    /**
     * @param $extension
     *
     * @return int|string
     */
    public function getMimeTyepForExtension($extension)
    {
        $extension = strtolower($extension);
        foreach ($this->mimeTypes as $mimeType => $extensions) {
            if (is_array($extensions) && in_array($extension, $extensions, true)) {
                return $mimeType;
            }
            if ($extension === $extensions) {
                return $mimeType;
            }
        }
        return false;
    }

    /**
     * @param $extension
     * @return bool
     */
    public function hasExtension($extension)
    {
        $extension = strtolower($extension);
        foreach ($this->mimeTypes as $extensions) {
            if (is_array($extensions) && in_array($extension, $extensions, true)) {
                return true;
            }
            if ($extension === $extensions) {
                return true;
            }
        }
        return false;
    }

    /**
     * Obtém a extensão associada a um mimetype
     *
     * @param string $mimeType Um mime type ex.: application/pdf.
     *
     * @return string|null A primeira extensão associada ao mime type ou null se não for encontrada
     */
    public function getExtensionForType($mimeType)
    {
        if (!$this->hasType($mimeType)) {
            return null;
        }
        $extensions = $this->mimeTypes[$mimeType];
        if (is_array($extensions)) {
            return $extensions[0] ?? '';
        }
        return $extensions;
    }

    /**
     * @param $mimeType
     * @return bool
     */
    public function hasType($mimeType)
    {
        return isset($this->mimeTypes[$mimeType]);
    }
}
