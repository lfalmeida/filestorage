<?php

namespace App\Libraries;

/**
 * Class Formatter
 * @package App\Libraries
 */
class Formatter
{
    /**
     * Converte um valor recebido em bytes para uma string formatada
     * adicionando um sufixo de unidade
     *
     * @param $sizeInBytes
     * @param int $precision
     * @return string
     */
    public function bytes($sizeInBytes, $precision = 2): string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];

        $sizeInBytes = max($sizeInBytes, 0);
        $pow = floor(($sizeInBytes ? log($sizeInBytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $sizeInBytes /= pow(1024, $pow);
        $formattedSize = str_replace(',', '.', round($sizeInBytes, $precision));
        return sprintf('%s %s', $formattedSize, $units[$pow]);
    }

    /**
     * Transforma de geolocalização extraídas de uma imagem com exif, na sua representação com inteiros.
     * Ex:( [0] => 46/1 [1] => 5403/100 [2] => 0/1 ) que significa 46/1 graus, 5403/100 minutos, 0/1 segungos,
     * em geral 46°54.03'0"N, que normalizando os segundos seria 46°54'1.8"N.
     * É necessário também informar o hemisfério para o cálculo.
     *
     * @param $coordinate
     * @param $hemisphere
     * @return int
     */
    public function gpsDegreesToCoordinates($coordinate, $hemisphere)
    {
        for ($i = 0; $i < 3; $i++) {
            $part = explode('/', $coordinate[$i]);
            $countPart = count($part);
            switch ($countPart) {
                case 1:
                    $coordinate[$i] = $part[0];
                    break;
                case 2:
                    $coordinate[$i] = floatval($part[0]) / floatval($part[1]);
                    break;
                default:
                    $coordinate[$i] = 0;
            }
        }
        list($degrees, $minutes, $seconds) = $coordinate;
        $sign = ($hemisphere == 'W' || $hemisphere == 'S') ? -1 : 1;
        return $sign * ($degrees + $minutes / 60 + $seconds / 3600);
    }
}
