<?php

namespace App\Repositories;

use App\Contracts\DocumentRepositoryInterface;
use App\Exceptions\DocumentNotFoundException;
use App\Models\Document;

class DocumentRepositoryRedis implements DocumentRepositoryInterface
{
    protected $redis;

    /**
     * DocumentRepositoryRedis constructor.
     */
    public function __construct()
    {
        $this->redis = app('RedisClient');
    }

    /**
     * Salva os dados de um documento
     * @param array $entityData dados que serão persistidos
     * @return string ID do objeto criado ou a string '0', em caso de falha
     */
    public function save(array $entityData): string
    {
        $key = $entityData['path'];
        $this->redis->set($key, json_encode($entityData));
        return 1;
    }

    /**
     * Recebe um array assossiativo mapeando os critérios para a busca de um único
     * objeto
     *
     * @param array $criteria
     * @return Document|null
     * @throws \Exception
     */
    public function find(array $criteria): Document
    {
        $key = $criteria['path'];
        $data = (array)json_decode($this->redis->get($key));
        if ($data) {
            return new Document($data);
        }
        throw new DocumentNotFoundException(trans('messages.error.DOC_NOT_FOUND_REPO'));
    }

    /**
     * Remove permanentemente um registro
     *
     * @param array $criteria
     */
    public function delete(array $criteria)
    {
        $key = $criteria['path'];
        return $this->redis->del($key);
    }

    /**
     * Recebe um array assossiativo para encontrar o objeto que se deseja alterar e um segundo parâmetro que
     * deve ser uma array assossiativo contento o novo objeto completo, que será salvo.
     *
     * @param array $criteria
     * @param array $newData
     *
     */
    public function update(array $criteria, array $newData)
    {
        $key = $criteria['path'];
        $oldObject = $this->redis->get($key);
        if ($oldObject) {
            $this->redis->set($key, json_encode($newData));
        }
    }
}
