<?php

namespace App\Repositories;

use App\Contracts\DocumentRepositoryInterface;
use App\Exceptions\DocumentNotFoundException;
use App\Models\Document;
use Illuminate\Support\Facades\App;

/**
 * Class DocumentRepositoryMongo
 * gerencia os documentos utilizando persistência com MongoDB
 *
 * @package App\Repositories
 */
class DocumentRepositoryMongo implements DocumentRepositoryInterface
{
    /**
     * Cliente MongoDB
     * @var \MongoDB\Client $client
     */
    protected $client;

    /**
     * Instância da collection gerenciada por esta classe
     * @var \MongoDB\Collection $collection
     */
    protected $collection;

    /**
     * Nome da collection que esta classe gerencia
     * @var string $collectionName
     */
    protected $collectionName = 'docs';

    /**
     * Nome do banco do MongoBD que esta classe gerencia
     * @var string $dbName
     */
    protected $dbName = 'db';

    /**
     * DocumentRepositoryMongo constructor.
     */
    public function __construct()
    {
        // obtendo uma isntância do cliente através do service container
        $this->client = App::make('MongoClient');
        // seleciona a collection que será utilizada
        $this->collection = $this->client->{$this->dbName}->{$this->collectionName};
    }

    /**
     * Salva os dados de um documento
     * @param array $entityData dados que serão persistidos
     * @return string ID do objeto criado ou a string '0', em caso de falha
     */
    public function save(array $entityData): string
    {
        $docs = $this->collection;
        $result = ($docs->insertOne($entityData))->getInsertedId();
        return property_exists($result, 'oid') ? $result->oid : '0';
    }

    /**
     * Recebe um array assossiativo mapeando os critérios para a busca de um único
     * objeto
     *
     * @param array $criteria
     * @return Document
     * @throws \Exception
     */
    public function find(array $criteria): Document
    {
        $docs = $this->collection;
        $result = $docs->findOne($criteria);

        if ($result) {
            $resultArray = $result->getArrayCopy();
            unset($resultArray['_id']);
            return new Document($resultArray);
        }
        throw new DocumentNotFoundException(trans('messages.error.DOC_NOT_FOUND_REPO'));
    }

    /**
     * Remove um arquivo
     *
     * @param array $criteria
     * @return \MongoDB\DeleteResult
     */
    public function delete(array $criteria)
    {
        return $this->collection->deleteOne($criteria);
    }

    /**
     * Atualiza um objeto
     * objeto
     *
     * @param array $criteria
     * @param array $newData
     * @return \MongoDB\UpdateResult
     */
    public function update(array $criteria, array $newData)
    {
        return $this->collection->replaceOne(['path' => $criteria['path']], $newData);
    }
}
