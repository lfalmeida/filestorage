<?php

namespace App\Exceptions;

/**
 * @codeCoverageIgnore
 */
class DocumentNotFoundException extends \Exception
{
    protected $code = 404;
}
