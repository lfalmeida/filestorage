<?php
/**
 * Created by PhpStorm.
 * User: lfalmeida
 * Date: 5/27/17
 * Time: 9:09 PM
 */

namespace App\Exceptions;

class MimeTypeNotAllowedException extends \Exception
{
    protected $code = 422; // Unprocessable Entity
}
