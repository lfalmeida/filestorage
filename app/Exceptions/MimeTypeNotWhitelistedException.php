<?php

namespace App\Exceptions;

class MimeTypeNotWhitelistedException extends \Exception
{
    protected $code = 422; // Unprocessable Entity
}
