<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class Handler
 * @package App\Exceptions
 * @codeCoverageIgnore
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        DocumentNotFoundException::class,
        DocumentTooLargeException::class,
        MimeTypeNotAllowedException::class,
        MimeTypeNotWhitelistedException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        $validEnv = env('APP_ENV') != 'local' || env('APP_ENV') != 'testing';
        if ($validEnv && app()->bound('sentry') && $this->shouldReport($e)) {
            app('sentry')->captureException($e);
        }
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        parent::render($request, $e);

        $code = 500;
        if ($e->getCode() && $e->getCode() > 300 && $e->getCode() < 500) {
            $code = $e->getCode();
        }

        $errorData = [
            'message' => $e->getMessage()
        ];

        if (env('APP_DEBUG')) {
            $errorData['httpCode'] = $code;
            $errorData['file'] = $e->getFile();
            $errorData['line'] = $e->getLine();
            $errorData['debugModeEnabled'] = true;
        }

        return response()->json([
            'error' => $errorData
        ], $code);
    }
}
