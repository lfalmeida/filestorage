<?php
namespace App\Exceptions;

class DocumentTooLargeException extends \Exception
{
    protected $code = 422; // Unprocessable Entity
}
