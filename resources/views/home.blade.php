<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FileStorage</title>

    <link rel="icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAgsUlEQVR4nO2deZxdVZXvv2vvc86dKhWS1JBAEkBplARBxeHTPGd9ok/R7o9WidL6nj3xXouKvnZ4tE3l2vp8aRQChKbx48f+NO2At0i3iqgoIgqK4sDQhBkSEAipDJVKqu5whr3eH+feSmWuW6lbVamq3+dzk6pb956zz15rr73W2msQjjWoCv39ho0bhWIx3v/P3dddV9Ds0PNE3WnOyonidKUiKwSWAIsEFiOSU9VAhEx6SWoiEqJaUdgJDCrsEPQPauQpk+iTKuZBqS58YusHPjBywJj6+jxWr1Z6ehwi2vI5mETIdA9gXFCE/pIBoLc3GX2/VLKdyZaTxJNXqNOzjDFnAi9QoUMwOcn4YAwkCaiCc6jT9GfY+7/I6P9iJP2OCFibfqcWobiKKNuBh51z94qR32msd22zyzbvPyYAenodwoxnhpnNAH19htWrZewELypduzAw8WvQ5HXAGxRZZTJBIIGPRjHECeoScAqQgFJ/TEFVQEAO8dyKglJfxTrmuxYjiLHgWcT30DDC1cJQ0AeAWxF7W+i8nw/2XjA0er1SybJxo1IsutZM0NFjZjJAqWTHitPu6y4taFvuTSScK/BW8e3xkgnQMEqJrupQHKKCYuorerKfTVEFwaGiCAYRI76HBD5aC9EoeVbhB1hulOHKLVs/8Il0u2hsW2MlxQzBTGIAoVTaZ5K6vrHuDHzvPIFeCbzni+/VJzpWkATBkD7DdD1HKikUB2rF90QyARrFaBg/rlAiiq8feN9F941+o1Sy9Pa6+nenHTODAdJJGSV85w1XnWOMuQDn3i75vE8YomGUEh21iMyMce8P1dExSuALQYCWyxHGfM85d+22d3/45tHP7vfM04Xpnci+PsOaNdoQ9V0brvlTEfcxMebVeB5arYHTuL7SzbSOtXmk25IRT7IZiGPUudtVzeUD7/pf/wGkW8OaNTKdOsL0MEBDq6+vgO4NV78NI58Qa1+LCFqpKoIDaYj4YxkK6lCM5LKCKpokP8PppVvf9aGbgLrOMz1Ww9RP7hjR19F/xUusmD7x/HdiTUp4UESOtdU+Pqg6QCSXFRKHxtF3EnXF7T0fvRuYlm1hKhkgNcNE3JKvrF1gF+YvFiMXSSaT1XKlseLtFI5nGqEJipF8TrRWq6rTdclQ+f/u+MtP7UHV7DVDW4+pWWmpc0QRcZ39V7zZW7zgDlPIfxolq5VKkip1c4X4AGIREa1UEpSsKeQ/7S1ecEdn/xVvRiS1EBoOpVaPpOV3aIi10pdy3TbzOTH24xhBa2ECYg7plJkrSJ1PTjKBxSnqksu2JrXP0Pu/K1OxJbRy8kft+o5vXPESk/P+2WQyr9ByxeGUWbvPTxSqDiNIPmdcrXaXq8T/c/v7Pnp3q/0GrWEA1fS6ItpVWn+++OZq8byFWqkmyFwS9ROAaiK5rNU4HtLIfWig98Kvj53Pyb7d5K9C7UuVGBHt2rB+rcn6X0N1oVbniT8uiFitVhNUF5qs/7WuDevXNuYT7Zt0ek2uBEg1WNdZWt8mRr9q8vkeLZcTnJoZ672bqVBVjDjJ560rl/vVyZ9v671wuDHHk3WbyeOoUski4jo2XLZMLD80+XyPjpRjlJnrup3JEBEUqyPl2OTzPWL5YceGy5Yh4ibTQpgcwpR6LL39Sfc3Lz9JPe9Gk8+driPlGBFvUq4/16EaSyHvuXLlfonjc7e+92ObG3N+tJc+egaomyqdpfWnGCs3ScY/VcvVeeJPNlRjyWc9rUWPuETftq33wscmw0w8OgbQPoMU3bLSupXO2J9INnPKPPFbiAYTVGuPGZe8cUvvRU81aDDRS05cB+hLb9xZWr80MfamlPi1eeK3EiKelmuxZDOnJMbe1FlavxQpOvombh1MjFj1Y9zu57cV1OgGk5vAnq86Q0IiZgCEvXGJR/6sp+VqbAr5012lsqH7ukvfvPX9f1sGzESOlZtngDS8SQCVXPZfJJ8/W4dHmia+ehb17PgffLZCFYkTJE6aYALxdKQcm7bC2Sj/AryH1asbh21NLavmGWDNGkuxGHd966q10p7vcXua1fYFF3j424cInt2OqYZND2E2wWUDwuM7iDoWYqIE1I2PEUQ8HS7HZkG+p6u0ftPAez78Kfr6POCAUPnDXqap0da1zq5vrnufKeS+rmEUo2rHfR0RUKX95/fS9rtHMLUQSWZswOyUQK3BZQKGzzqV3a85M50jp+OdUUUkkcD33Ejl/IH3XvSNZi2D8TNA3QPV1b/uDLHB7SK0aZzAeBVJVdT3WfTDX7Hgl/fjcpk0/n4e4BymErL75S9g8O1nY5pbFE48iyrDmoSvHui56L5mvIXj5LM0dm356vZMKJk7TDZ4qVaqbtwneqpo4JN58jm6/u1H6d7PvBI4ikYUexjx3FteRvSyVdjEjX96VJ3kssZVw98HWnvV0xt318bGWh4O4yNgf6+hWHQ1533eFLIv1Uo1buo4V0E9S/bxZ5EoTh94nvh7UV8LRoTMw08Sbn4qfX/8SqHRSjU2hexLa877PMWio793XPQ58ofqLseu6698k8kGH9NKJWk6ekdA4gRbrh77IZ4thqeg23cSPfFk+sa4rSSxWqkkJht8rOv6K99Eb39CqeeIdDo8A6gKG1dp983XFbBypYiBRGXCUTw6vmUvIhhz+APE2RAufFA4RTxLsmOQ6InN6XvjsgoQEhURA1au7L75ugIbV+loLMEhcHgG6O83FItOhwY/Ywq507RWS1odyWOMIYoiypUKSZJg9lMUG2lAFafUnE5rWlCroAp4Ftc0E4jRWi0xhdxpOjT4mXQr6D8svQ5tv6eaZNLdv361GrlIKzVXT9BoKUZGRlixYjmdHR08sWkzQ7t3k8tmUU2JHdWFyGkFHwc8OhJhRPBklqkVY5kA8J930qgZfVgIRis1h/Uu6u5f/7WtPb0bD2cVHJoB1qxJx+HiL5h8IavlSlJP1GgJjDFUKlU++N/fzwV/+Rfk8lmeefpZip//Anf95rcU8nnCJKHdM6x5fjuvWpRBgdt21vjs47upOKVuW8weKOB5TTKBCC5xJp/LuuGRLwDvaNDyYDg4QUs9lmLRdd5w1Tkmmz1Xy9WWxuyLCOVKhRe/+Aw+8fGLaG9fgDWWk046keIlf0ehUMAlCTWFDx6f541duTT7W+EtS3OcvyxPOdHZ6VVW3csE494OxGq56kw2e27nDVedQ7HoDqUQHpwBNq5StM+YRC9JnTXj1N4mCGMMYa3GGaevxjlHnKSpAs45lp9wAstPOIFaFFGwhtMXBCSJooAD4lh50QKPrJF6SYBZiAkxgSrGYBK9BO0zbFx10Nk5kAE0Xf1dN3SeK7nM2VqttT5jRxXP99m0+UmMMXjWjiqAg7t2MbBtG77nUU4cT1VjrBUaqTOeJzxZcdScYmajBGigaSYQq9Wak1zm7K4bOs+lWHTogVLgIBKg5OjrM6LuU6l63drVD5A4Rz6X45d3/opvlvoB8DyP4eFhLv3SOnbu3InveXgCX316hAeGQgpWKFjhvsEa1z07QsZMxUinGU0zQao5i7pPpTEDpQMUwX2VwDSwM+m6/so3SuD/sVZqOpWh3MYYvrD2i/z4lltZsfwE7r3vfh597DHy+TyxcwQi/KHq+OuNg7zyuABV+NVQyHDsCMwxUJBnMjCWCTiSYihWKzWVwP/jrtOWvGFA5Jb9D4v2ZYCNGxVADB/G9yCMHDBlDGCtxVrLr+/6DXf84k4y2Qz5fB7nUsZVIGNgJFFu2lYFIG9l7hC/gaaYAIfvWQmjDwO3NGjcwF4GqMeWLS1dsUqNvFUrtSlP39L6A+Tz+VElsEH80c8AVqDdq+sBc/VMabxMIGK0UgMjb11aumLVc70ffWBsHOFeAvevFgAn8mcmn/dxLmGSnWzjJZRzjiRJRhniYNdJND02n5PEb2B8OoHgXGLyed+J/BkwSmvYywBCb2+y7LvX5kHP0zA86JUmPtA08EGzmTrFZrO6fhRQSHyLShPHpeNiApGUpnresu9em6/rAAINBtC0CKML4zeaIHOyRvHkun0FxCm1lV2onQ8CORgEUCNUFxearxRzJCYQjEaxM0HmZBfGb0y/k9I8pUZ//Tq4PxHfr5c9m0SIIGFE5ZTllF+4EjtchkZFTiN7f56Vrrz9IILu90IEW4sY7l7ISMcCTDzOuMCxOBITKE58XxX3J8AozUcjSReVrl0YUHsQz1tGkjgmO3NYFbUWU62x6Pu/Jv/Qk0iU7N0N6pHCzpvdCcTGKcbp6BoXwFnDcHc7A6uW43yLHI1DQwTiGLNkUaoYQl0xFIc1hjjeEpI5bbD3giFUxeO2NRaIrYSvFt9fpmHUmiJNIkiSoNkMO971WkaeeJbM0wNILUJUcZ4lt3UX+Wd2pCFjs82rI2Ac7FkQUG4LUiYQcJ6lelyecseCtErt0T73oa0DQxyrBP4yG4WvBr7HbWusx22N8enryQRKGCdMNGHkSBCBup+/+kcnUHnhSnAOcUqcz9B150N0DIwQ5wNkljn2VcCLHNuWL2LrioV4kUMlfV+cNhsIeoSbHYoJSMhkrETR64HvcRt4FIsx2mfMDbyBMBKkXlC5VajvS+nKDwFJJ0BAohhFU/NvlkmA1GehSOywUYypM0Cq77dgyg/GBAYhDMXAG+q+gNgAdF6/+GSU1RrFtPLMfx/I/kqgzBElkFHFj1ElsEX32l8xVIzGMaqs7rx+8clQV/TUmpdLNuPj0kKGLRrOoQc5C1f8jMFYJtj0lODUmWzWV9GXQ50BrPAyCXzSvKQpGhSAMaM5gupZRs9zZ7MgEEHNmFdD6rWS/0eZYCfhps1OMh7W2pdBQ9lTztQoZkpKudSTRHAOO1JBwhhxilQCZKSKU4fG7pBu4GMWAk4dEkb4IzW8uB7U4hmSwEOt1O3/Ft2/wQTbd0n4yBN4S7vPRATpvu7SguayDxrPW6FRpC1lgkaG0KYtLLhzI8GWHciYwx5xCm7qd6GpggBuvzBmNUKtPcfgyV2Ul7S1lgnqt0QQaWt72py2apWnnv98Ubo0aSI9eUK3VVwQUPjPx1n83TswcYLzvAMfthk/+DEGBaQRy1aHJI7CwB5yO4bZ+qIV7Dl+EaaZVPEJQdCh3Uv1/ntXG/G9F2IkU9+XW1Q4Mk0N83cOcdyPfoMouGxmr+Y/9jUXIGNfUvf+QeeDzxKUa9Da8xLBqYrveerkecahKyUTALSuJm09Mzj38FN4u0dQ36uL+nkAqXS0Bq8WUdg6hLOtjm/TBN9HSU72BE6sp2m37o5pXBre4J7GAFp2q2MdfjlsvUkspPdIdLlRZUXaV6/FWT9O02IQ4054nSPbwX6Q0SiXVupjGJIE4HnGoEuaqEjRcjSSQp1zh0wQHRv+PatDwVsJBU3idqOwqO6Jm/apFBEqlQq1WojneYyMjBDH8QFMMBynIlLrP0/7wI81iIiqIsZ2eiCLdRo8wAcZE5VKlTNedDp/9Rf/g+XHn8Dv77mba778FXbtGsL3fVSVWOF9S/O8vSuLA76ztcK3BypzLzL46CCoQ4zX4Ylofrq3ABEhiiKWn3A869d9kSVLlgBw6qmn0N3dzUc//gkMsCdR3r00x9+furDRGpazjguoOOXGbVXarbTQlJllUFA0a1Tx629NGwsYYyhXKrzqVWezZMkSoigmqUcGv+ZV/4UTV66kGoZkreEtHVmcU4bj9KUKb+nM4guTHMc2B+DUMyJkZsJJnBGhWq2O/q7OISIkSUIUpTUAHIzmADZyA0WEWlIPYpxXBpqDczMjRDdJEvL5PD/72e089PAj+L6H53kYY/j3b3+XPzz9NJkgIHKOG56rEDllgW9YEBiqsWPD1srePt/zaAqeKjUxkp9uKWCtZfeePXzoox/nfef1sqy7m3vuu48N//EdgiAgUSVvhJ8N1vibBwb5bx05HHDjQIXf7A7J21mcHt46qCdC1PiFaVxEqkoQBGzfvp21X7wM3/OI45i2tjastaPHwzkj/GIw5I7BsF5aDQqzuTZAKyGSeKpSFisLiaffGaSq+L5PJpNJ4+fqOsDY2AAFCt6+A50n/gQhEhrQnWkU+MyYRVUlSZLR/MCDwem+r3k0DcUYFLYZgcH6UWxLp1KN1OP9W3mXYx9az5BqqVtLVcUIqNtiHLJj1K5q3Q1BhKhzUf3Mf15fPxTCBZkpmx6xZsSI8AesJe3e3ao7GUwtonzaSqLOhUg1TIMe5mIwyP6QVDqaKCFsyzLcfRwmTvYGi7bknuIwFhybjcKT9eCM1t1RAOdwhRw73n42SXseM1xBohiJ668onhtBIqppEGzjlTi8WkyS89l6+nKSjNd6xUa1kZD7B88gT2kthFaXgqlnCIcnLmXgA2+h7XcPETy9DVNLG1yoAa8aYSshLc5NmhY04gHjwBB7hobG5TxLdVGeoZVLCPOZiWUGNw+rUYwaedLTKH5IPFurxwW21hdQZ4J4URuDb3klphqOroQ4H9Dxm0dZescDxFn/6JMkZxjS3EDl6RMXsvWE9jG5gYLzTZoe1/qIYEgtACFxkcsED3kSR4+rbweMtSs0ilrPfSL7NElSz4BLU8NHgyGlxUrptCFVhtUK6gStB0CbqB4FPFViL4283mUi85DZ+oFPjIjyMGkHr6nZhMcqfWNTw2Yl0Q8CHfOCqVWAVVWsQcU8MsRxu9MlJ9wrvsfsS8eZx0HgSFv23Eux6DyARPmthBFTlhncQIPftFHyaw7w32htuzE1QqZS4xXSgxPL76CeGyiJ+41WaxHG+C1XBCF9eGtQ69Fok+ZyAfgegiAis88tIGnsglqD8y2OMTpAw6c9FQogYrUWOlPI3gV1Bth23s5N3Tcs2Si+92INw5YXh9ZMgN1Txts6iK2lcfCZwMM+s40wiXChzr7wHgGXOPxdw7Rbl1YEqZeIqS3IEmf91ApoJRTwLDh9bDBc9BCAR1+fhxRj13/lrTbwz6QWtTbM1loW/Gojbb9+ALunMsbcU1SEsjH1tiCzTQSkyD01Qv7JMW8IxBmfwZM7GFrR0WLzVxMJPE9r0a0UizGlHmt4Xf1PyE+phQLamtWvivM92n9+D4tuuhNvT1oqTj1Tf9kxjSRnJ/GBg9YH8KoR3fc/w+LHt9bTwlp2d0OcoL53KwAbV4nhdWsSgESD2zWMtuB5wmQLYAX1fTLPbGPBnWnXULV29G/7mERzEGqFxLcs2jRAdle5VcU0FWuMxvFgUgh+CkCxmBhElFLJDvZeMKRGfmCymckvFKmKeobsY89gqlG60ueCxj9eaOoRNJGjsG03amXytwLVRHxfMXLz8N9etj3tH4CmrNaTfkYw39YokknvDlYvhWb3lBujmdTLzxoIeLUoXTCTvw0aVMUZ7zujvzf+QXrT0uGB9xMX1jaJ75lWSAHR8Zk6qRl4+A/O2qiCVmyHimKtcVG8xebafgBAsbhP52+lVLJb3nFBGeR6CQKmS0YbY0iShDCKRn8fiwbhI01fs7aD6KRCnQQ+xpr+wU+vHaKnZ7TD3t7Z7Uk7SRjVr7lyOcKYKW/DJyKMjIyQy+VYtrQbUCrV6qg0ECBWqDqlIzAsCYSyU5Lpj2ed2RAxGkaJE/OvAKza20Fsb0lYKTr6+sxzvR99oLt05Q+kkH2HjlTcVPUMMsZQLpd55zvezocu+GsWLV7Eo48+xj98YS0PP/wouVyW2DkyBi4+qZ1zOrIocNNAhcufHKZR3Xpeu9gPSiIZ32gU3zZUvPr39eLgo9v7vvJ1ddpJQh1XEcUH/r1FkHpa2KrTXsjn1lzC8uUnUMjnefGZZ/D5Yh+ZTADOUU3g/cfn6V1eIGeEvBHOX9HGe5bmKCezvG3cRJGm/QuBvQqA3n3byu9L4N7eBFUZeHDHrRpGd0ouI6AtT7g1xlCpVnnZWWdhjCGO4zQ83DlO/aNTWLlyBdUwJO8JL18YkMRpmnikkCTKKxZmCMx8iPgBUHUS+OKi6L5drvMmQOjv34eeB1nhvWnHcDFr66VKpmRdedZjy5Yto63j4zjGGsPwSJldO3fhe5Zq4hioOazZmxNgrTAQJkRTEkl1DMIaMZ6/lmIxpqfnAHofyADSn9DXZwbeve1GrdR+KdmMabUUSJKEQiHPz+/4BTf/+BaMMfi+j1Pl6muuZeu2AXzfxwD/+myZZ8oJbfXk0M0jEf/2bBlvnvj7QtWJ7xsNo3sG6SiRrv4jNI5sYPUDgvQn7oarPmud++FUrS3nHBf//Rp+/JOfcuKK5fzunnv57W9/Ry6XI04cWSM8NBLx5/fv5HWLMzjgpztrPFdLZnfv4IlARDGCsd5n6SvG9PTY/cU/HIoBelMpsO3dH76561vrbjRthXPr7eNbahF4noeq8r2bvk8Ux2QzGXK53GigkiNNDn0udHz1mREACtaQnS8Psy9UE8kEVqP4J4Of++dv4zpMw/GzPw6t5dd7zovx/o+rhlWMlVY7hxqEbm9fQMeSxeTz+QOi1BzgCyzyDYt8gzdb40cnDkVENHGxzeU+faSUv0MzgIijVLJbey7cSBKvk1xm8t3Dh0CSOOI4OaBraANK2jgymUNxpOOG4iSbMap6zY7PrPstPWk3+EN9/PB2fk9P2kl84aLPuZHKg5LJWHQCPQWUNBQq8FtfBPFYhqYRQpgJJoeqOvGs0TDajM1cgqqM9fodDIdnABFl9QOy9ZwPjJDoR1QdWFG0+dGJKuHyTtTM3mrgRwMBMEL1uPxEp0fTnD8j+PKRoeIVu+hNTfrDfenInr7e/oRSjx047yO3uGp4ueRytmmz0AhSi6icuoLqycsw5RpY2ad3zlx9jTaODCPKi9sY6WqfWHKoaiLZjKfOfXlX3zU3Ujq41r8/xncXVWHNGlm+uj0TSuYOkw1eqpWqo5n+gqqo52GH9rD4339O9untmLRIwRxHGv9YXlJg64tWEud8pNnTrYbNr8mDtq39lTvK+RHWrNHx1HwY/21UDSKuq3/dGWKD20Vo03ifI+XxXCMtFV+ukLn9HjJbdtS7Vs1dNlBrqSzKM9x9XBoJ1DTxUYwo1tRcYF+3+zPr7zqUzX8wNCdnSiVLb2/S9c117zOF3Nc1jGJUbVPXUQVrSZKI6JEnoFwBOws7hY4TqU4kaZeQCXwdJZFs4DmnfzVUvPorzRAfJqKO9/V5FItx17euWmvb8590e8ox0mSnUVXEGFytRvToJrRaBVNngrlmIDQYfyLOVtVY8lnP1cJrhj537d/UF6ijCTWy+buqCv39hp4et/SG9d+ikO/R4ZEYkSaZgDQ7qFojeuwJtFKd05KgaajGkst6Lo5/MPT6rnfwTw8opX7XrP3Y/Hm/iLIxjR7SSvWDrlz+pRTyHqpxc9cBEodkM/inPA/JZaHVjatmC5REMoFHHN9NjvN5fTFh1SqdiPNg4rPd12coFl1naf1SMfpjk82cruXaBLYD5iVBM1BNJPCtOn1cCv4bBj995VMNWkzkcke33NIGxG5Zad1KZ+xPJJs5RcvV+e2gVagT3zl92s/l3rj94sseaVbp2x9HF/IlRUepZLf0XvSUc/JWrUWPSD47vx20AnXiIzxlsv5bt1982SPjdfYcDkcf89fbm1Dqsdt6L3yMMDzHVWr3z+sEkwzVWDKBVaePxcZ/8+DfXXk/pR5L79ERHyYr6LO3P6FUslvf+7HNzkRvdtXqL6St4AHzTHC0qGv7mri71QT/dc8lVzxMz+QQHybb6q57CztL69vE6FdNPt+j5XKC04O3/zrkdZjXCdJq2U6yGeui+Ptqsu/fXbx852St/AYmOQdQHNpntvVeODzw7g/3ukrlHyUILJ5tLrp4rksCpw4jQiawLkmuHjJd79xdvHwnfX1mMokPrfK7NVrQiWhXaf354purxfMWaqWaNJVoMhclQcPMU62IZy4avOTqLwNCX59M1NQ7HFqT+CGiiECpZAd6L/x6Uole7+LoLmnLWwQ37qCSuSQJVB2qTnJZC/ynCG8avOTqL1PqSRdMC4gPU+F5rx8gUfpSrttmPifGfhwjaC1MQAwyjjHMdkmgmohnrVoLzn1Z8u2fHPz02qHJ3u8PhqlZSg0mADr7r3izsf6lks2coeUKqBtftPFsZAIlQTCSzYjGySNYPrmr75/S/P2jdPCMF1MpS6WRmLjkK2sX2IX5i8XIRZLJZLVc0bRc/REYYbYwQX0LlExgNI5VjXeVyy747J6L/9+Oult3yormTP1mOkYadPRf8RIrpk88/51Yg1aq6YMfLtLoWGaCBuED3yCCuuRHIlIcLF7zS4CpEPn7Y3q0KUXoL5kGI3RvuPptGPmEWPtaRFJGSCXCwes/HFtMoIBD1UjgC0ZQ1btUvH8c6rtyA5AerK0pTug072gxvep0X58ZG7vWteGaPxVxHxNjXo3nodUaOI3rNYv2lQoznQnSjJYEEU+CtDuvOvdbEXP5IB3X17X6lpl348XMsKfGbAsAnTdcdY4x5gKce7vk8z5hiIaRgiSgdtSrOPOYoF5hTQXPGvF9tBY6PPtjNd61Q6dt+e6oiJ8GcX8wzAwGSCGU9m4LAF3fWHcGvneeQK8E3vPF99BaiEZxygySVr7CWpkmJtD6Sk8LlFhjxPfTophR/CxGNtiM97Wdn1l/1+g3UsI3FbbVSswkBtiLUsnS0+MaW0P3dZcWtC33JhLOFXir+PZ4yQRoGKFxDIlzGOu0WpXosSeMVmtp4YDJZYL0Yg2Cp1LIYi0SeBAnaJzsVGt/aERucnjfHypesav+XaHUY2YS4RuYmQzQQF+fYfVqGSsVFpWuXRiY+DVo8jrgDYqsMpkgEN9HnUNHyoQPPYaOlMGaJJ3uUdd0GoJ7eDRWderR1HoxLWMEEcQa8DxwDg2jGOEJNfYWI3J7SPbWkeKXBkav1NNjWbVKp3OPPxJmNgM00LAagLHMQKlkO5MtJ4knr1D0LKOcibUvcNWwI9n0ZE4bnUeTOh84d+Q6MkaQRmk6Y8AYNIohSUKMDCk8ipi7jeo9tGXvHHzewkfpLYaj3+/rM6x+QGbiaj8Yjg0GGItGVPLGjUKxeEC8Qfd1lxbo7j453rz5jPjpLacKLEXkZFzcrsgJIrIIJUDVQ7VhWThEHCKhCttwbotYM4LKJhV9Fmc2uax9yETmoaE164YOyLgp9Vg2rhLWFJPpMOWOBv8fg+TuvtZ14/YAAAAASUVORK5CYII=
">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #c0c0c0;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            FileStorage
        </div>
        @if(env('APP_ENV') !== 'production')
            <div class="links">
                <a href="/_docs">Documentation</a>
                <a href="/_apidocs">API Explorer</a>
                <a href="/_example">Upload de Exemplo</a>
            </div>
        @endif
    </div>
</div>
</body>
</html>

