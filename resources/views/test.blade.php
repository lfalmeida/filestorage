<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Styles -->
    <style>
        /* Space out content a bit */
        body {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        /* Everything but the jumbotron gets side spacing for mobile first views */
        .header,
        .marketing,
        .footer {
            padding-right: 15px;
            padding-left: 15px;
        }

        /* Custom page header */
        .header {
            padding-bottom: 20px;
            border-bottom: 1px solid #e5e5e5;
        }

        /* Make the masthead heading the same height as the navigation */
        .header h3 {
            margin-top: 0;
            margin-bottom: 0;
            line-height: 40px;
        }

        /* Custom page footer */
        .footer {
            padding-top: 19px;
            color: #777;
            border-top: 1px solid #e5e5e5;
        }

        /* Customize container */
        @media (min-width: 1024px) {
            .container {
                max-width: 1028px;
            }
        }

        .container-narrow > hr {
            margin: 30px 0;
        }

        /* Main marketing message and sign up button */
        .jumbotron {
            border-bottom: 1px solid #e5e5e5;
        }

        #bar {
            -webkit-transition: width .5s ease;
            transition: width .5s ease;
        }

        .string { color: #4caf50; }
        .number { color: darkorange; }
        .boolean { color: blue; }
        .null { color: magenta; }
        .key { color: #c62828; }


    </style>

    <script type="text/javascript">

        function syntaxHighlight(json) {
            if (typeof json !== 'string') {
                json = JSON.stringify(json, undefined, 2);
            }
            json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
        }

        function fileSelected() {

            document.getElementById('codebox').style.display = 'none';

            var barEl = document.getElementById('bar');
            var fileNameEl = document.getElementById('fileName');
            var fileSizeEl = document.getElementById('fileSize');
            var fileTypeEl = document.getElementById('fileType');
            var filePercentEl = document.getElementById('progressNumber');
            var resultEl = document.getElementById('resultJson');

            var file = document.getElementById('fileToUpload').files[0];
            readURL(file);

            barEl.style.width = 0;
            fileNameEl.innerHTML = '';
            fileSizeEl.innerHTML = '';
            fileTypeEl.innerHTML = '';
            filePercentEl.innerHTML = '';
            resultEl.innerHTML = '';

            if (file) {
                var fileSize = 0;
                if (file.size > 1024 * 1024)
                    fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
                else
                    fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

                fileNameEl.innerHTML = '<strong>Nome: </strong>' + file.name;
                fileSizeEl.innerHTML = '<strong>Tamanho: </strong>' + fileSize;
                fileTypeEl.innerHTML = '<strong>Tipo: </strong>' + file.type;
            }
        }

        function readURL(file) {
            if (file) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var preview = document.getElementById('preview');
                    preview.setAttribute('src', e.target.result);
                    preview.style.display = 'inline-block';
                };

                reader.readAsDataURL(file);
            }
        }



        function uploadFile() {
            var fd = new FormData();
            var input = document.getElementById('fileToUpload');
            var xhr = new XMLHttpRequest();

            if (!input.files[0]) {
                alert('Selecione um arquivo.');
                return false;
            }

            fd.append("fileToUpload", input.files[0]);
            xhr.upload.addEventListener("progress", uploadProgress, false);
            xhr.addEventListener("load", uploadComplete, false);
            xhr.addEventListener("error", uploadFailed, false);
            xhr.addEventListener("abort", uploadCanceled, false);
            xhr.open("POST", document.getElementById('form1').action);
            xhr.send(fd);
        }

        function uploadProgress(evt) {
            if (evt.lengthComputable) {
                var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
                document.getElementById('bar').style.width = percentComplete.toString() + '%';
            }
            else {
                document.getElementById('progressNumber').innerHTML = 'status do upload indisponível.';
            }
        }

        function uploadComplete(evt) {
            console.log(evt);
            var block = document.getElementById('resultJson');
            document.getElementById('codebox').style.display = 'block';
            block.innerHTML = syntaxHighlight(JSON.stringify(JSON.parse(evt.target.responseText), null, 2));

        }

        function uploadFailed(evt) {
            document.getElementById('resultJson').innerHTML = evt.target.responseText;
        }

        function uploadCanceled(evt) {
            alert("Upload Cancelado.");
        }
    </script>
</head>
<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">

            </ul>
        </nav>
        <h3 class="text-muted"><strong>Exemplo:</strong> upload com barra de progresso.</h3>
    </div>

    <div class="jumbotron">

        <form id="form1" class="form" enctype="multipart/form-data" method="post" action="/teste/avatars?key=mySecretApiKey">
            <div class="row text-left">
                <div class="col-lg-4">
                    <a href="#" class="btn btn-md btn-default btn-block" onclick="document.getElementById('fileToUpload').click()">Selecionar
                        um arquivo</a>
                    <input type="file" style="display: none" name="fileToUpload" id="fileToUpload"
                           onchange="fileSelected();"/>
                </div>
                <div class="col-lg-8">
                    <div id="fileName"></div>
                    <div id="fileSize"></div>
                    <div id="fileType"></div>
                    <img src="" style="display: none;max-height: 100px;max-width: 100px;" alt="" id="preview">
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-lg-4 text-left">
                    <a class="btn btn-lg btn-success btn-block" onclick="uploadFile()" href="#" role="button">Enviar</a>
                </div>
                <div class="col-lg-8">
                    <div id="progressBox" style="padding: 3px;margin: 8px auto auto;background: #eee">
                        <div id="bar" style="background: #81C784;height: 2px;width: 0"></div>
                    </div>
                    <div class="text-center">
                        <div id="progressNumber"></div>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <pre id="codebox" style="display: none"><code class="json" id="resultJson"></code> </pre>

    <footer class="footer">
        <p>FileStorage</p>
    </footer>

</div> <!-- /container -->

<div class="container">

</div>


</body>
</html>

