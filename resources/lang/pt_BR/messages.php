<?php

return [
    'error' => [
        'DOC_NOT_FOUND_REPO' => 'Não foi possível localizar o documento solicitado.',
        'DOC_NOT_FOUND_ROUTE' => 'Não foi possível localizar o documento solicitado: %s',
        'INVALID_PROP' => 'Propriedade inválida: %s',
        'INVALID_MIME_TYPE_EXCEPTION' => 'O tipo de arquivo %s é inválido para este recurso. Os tipos permitidos são: %s.',
        'INVALID_DOCUMENT_SIZE' => 'O tamanho do documento (%sMB) é maior que o permitido: %sMB',
        'NOT_REMOVED' => 'Não foi possível remover o documento solicitado.',
        'IMAGES_ONLY_PARAM_EXCEPTION' => 'Os parâmetros fornecidos são permitidos apenas para imagens: %s',
        'MIME_TYPE_NOT_WHITELISTED_EXCEPTION' => 'Arquivos do tipo %s não são aceitos.',
        'EXIF_EXT_NOT_INSTALLED' => 'Extensão exif não disponível no servidor.',
        'BAD_METHOD_CALL_IMAGE_ANALYSIS' => 'É preciso realizar a análise da imagem antes de invocar o método %s.',
        'UNESPECTED_EXTERNAL_API_RESPONSE' => 'Não foi possível interpretar corretamente a resposta da API externa.',
        'FAILED_ENCODING_IMAGE' => 'Não foi possível codificar a imagem para enviar para a API externa.'
    ]
];
