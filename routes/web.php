<?php

$app->group(['middleware' => 'throttle'], function () use ($app) {

    $app->get('/', function () {
        return view('home');
    });

    if (env('APP_ENV') !== 'production') {
        $app->get('/_example', function () {
            return view('test');
        });

        $app->get('/_example2', function () {
            return view('test2');
        });
    }
});

$app->get('/swagger.json', function () {
    $swagger = \Swagger\scan(realpath(__DIR__ . '/../app'));
    header('Content-Type: application/json');
    echo $swagger;
});


$app->post('/{resource:.*?}', 'DocumentsController@upload');

$app->get('/{resource}/{path:.*?}/meta', 'DocumentsController@showMetadata');
$app->get('/{resource}/{path:.*?}', 'DocumentsController@showDocument');

$app->delete('/{resource}/{path:.*?}', 'DocumentsController@deleteFile');

$app->get('/{path:.*?}', function ($path) {
    return response()->json([
        'error' => [
            'message' => sprintf(trans('messages.error.DOC_NOT_FOUND_ROUTE'), $path)
        ]
    ], 404);
});
