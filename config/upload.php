<?php

$whitelist = [
    'text/*',
    'text/plain',
    'image/x-icon',
    'text/markdown',
    'application/pdf',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
];

$mime = [
    'images' => [
        'image/jpeg',
        'image/png',
        'image/gif'
    ]
];

return [
    'resources' => [
        'avatars' => [
            'allowedMimeTypes' => $mime['images'],
            'maxSize' => 300
        ],
        'women' => [
            'allowedMimeTypes' => $mime['images'],
            'maxSize' => 2
        ]
    ],
    'default' => [
        'images' => $mime['images'],
        'whitelist' => array_merge($whitelist, $mime['images'])
    ]
];
