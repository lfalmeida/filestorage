<?php

return array(
    'dsn' => env('SENTRY_DSN',
        'https://09124fdc97a943d28a1c1aecf71db72b:a276812e5259451986533f7a567d9019@sentry.io/178316'),

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
    'user_context' => true,
);
