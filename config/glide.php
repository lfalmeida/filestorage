<?php

return [
    // keys disponíveis via query string
    'keys' => [
        'or',
        'crop',
        'w',
        'h',
        'fit',
        'pixel',
        'bri',
        'con',
        'gam',
        'sharp',
        'blur',
        'pixel',
        'filt',
        'mark',
        'markw',
        'markh',
        'markx',
        'marky',
        'markpad',
        'markpos',
        'markalpha',
        'bg',
        'border',
        'q',
        'fm',
    ],
];