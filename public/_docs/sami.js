
window.projectVersion = 'master';

(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:App" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App.html">App</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Console" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Console.html">Console</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Console_Kernel" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Console/Kernel.html">Kernel</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Contracts" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Contracts.html">Contracts</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Contracts_DocumentManagementServiceInterface" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Contracts/DocumentManagementServiceInterface.html">DocumentManagementServiceInterface</a>                    </div>                </li>                            <li data-name="class:App_Contracts_DocumentRepositoryInterface" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Contracts/DocumentRepositoryInterface.html">DocumentRepositoryInterface</a>                    </div>                </li>                            <li data-name="class:App_Contracts_UploadServiceInterface" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Contracts/UploadServiceInterface.html">UploadServiceInterface</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Events" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Events.html">Events</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Events_Event" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Events/Event.html">Event</a>                    </div>                </li>                            <li data-name="class:App_Events_ExampleEvent" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Events/ExampleEvent.html">ExampleEvent</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Exceptions" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Exceptions.html">Exceptions</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Exceptions_DocumentNotFoundException" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Exceptions/DocumentNotFoundException.html">DocumentNotFoundException</a>                    </div>                </li>                            <li data-name="class:App_Exceptions_DocumentTooLargeException" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Exceptions/DocumentTooLargeException.html">DocumentTooLargeException</a>                    </div>                </li>                            <li data-name="class:App_Exceptions_Handler" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Exceptions/Handler.html">Handler</a>                    </div>                </li>                            <li data-name="class:App_Exceptions_MimeTypeNotAllowedException" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Exceptions/MimeTypeNotAllowedException.html">MimeTypeNotAllowedException</a>                    </div>                </li>                            <li data-name="class:App_Exceptions_MimeTypeNotWhitelistedException" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Exceptions/MimeTypeNotWhitelistedException.html">MimeTypeNotWhitelistedException</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http.html">Http</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Http_Controllers" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Controllers.html">Controllers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Controllers_Controller" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/Controller.html">Controller</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_DocumentsController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/DocumentsController.html">DocumentsController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http_Middleware" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Middleware.html">Middleware</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Middleware_Authenticate" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/Authenticate.html">Authenticate</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_SimpleApiAuth" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/SimpleApiAuth.html">SimpleApiAuth</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_ThrottleRequests" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/ThrottleRequests.html">ThrottleRequests</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Jobs" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Jobs.html">Jobs</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Jobs_ExampleJob" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Jobs/ExampleJob.html">ExampleJob</a>                    </div>                </li>                            <li data-name="class:App_Jobs_Job" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Jobs/Job.html">Job</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Libraries" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Libraries.html">Libraries</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Libraries_Formatter" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Libraries/Formatter.html">Formatter</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Models" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Models.html">Models</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Models_Document" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Models/Document.html">Document</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Providers" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Providers.html">Providers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Providers_AppServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/AppServiceProvider.html">AppServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_AuthServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/AuthServiceProvider.html">AuthServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_EventServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/EventServiceProvider.html">EventServiceProvider</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Repositories" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Repositories.html">Repositories</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Repositories_DocumentRepositoryMongo" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repositories/DocumentRepositoryMongo.html">DocumentRepositoryMongo</a>                    </div>                </li>                            <li data-name="class:App_Repositories_DocumentRepositoryRedis" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repositories/DocumentRepositoryRedis.html">DocumentRepositoryRedis</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Services" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Services.html">Services</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Services_DocumentManagementService" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Services/DocumentManagementService.html">DocumentManagementService</a>                    </div>                </li>                            <li data-name="class:App_Services_UploadService" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Services/UploadService.html">UploadService</a>                    </div>                </li>                            <li data-name="class:App_Services_UploadValidationService" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Services/UploadValidationService.html">UploadValidationService</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Transformers" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Transformers.html">Transformers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Transformers_DocumentTransformer" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Transformers/DocumentTransformer.html">DocumentTransformer</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "App.html", "name": "App", "doc": "Namespace App"},{"type": "Namespace", "link": "App/Console.html", "name": "App\\Console", "doc": "Namespace App\\Console"},{"type": "Namespace", "link": "App/Contracts.html", "name": "App\\Contracts", "doc": "Namespace App\\Contracts"},{"type": "Namespace", "link": "App/Events.html", "name": "App\\Events", "doc": "Namespace App\\Events"},{"type": "Namespace", "link": "App/Exceptions.html", "name": "App\\Exceptions", "doc": "Namespace App\\Exceptions"},{"type": "Namespace", "link": "App/Http.html", "name": "App\\Http", "doc": "Namespace App\\Http"},{"type": "Namespace", "link": "App/Http/Controllers.html", "name": "App\\Http\\Controllers", "doc": "Namespace App\\Http\\Controllers"},{"type": "Namespace", "link": "App/Http/Middleware.html", "name": "App\\Http\\Middleware", "doc": "Namespace App\\Http\\Middleware"},{"type": "Namespace", "link": "App/Jobs.html", "name": "App\\Jobs", "doc": "Namespace App\\Jobs"},{"type": "Namespace", "link": "App/Libraries.html", "name": "App\\Libraries", "doc": "Namespace App\\Libraries"},{"type": "Namespace", "link": "App/Models.html", "name": "App\\Models", "doc": "Namespace App\\Models"},{"type": "Namespace", "link": "App/Providers.html", "name": "App\\Providers", "doc": "Namespace App\\Providers"},{"type": "Namespace", "link": "App/Repositories.html", "name": "App\\Repositories", "doc": "Namespace App\\Repositories"},{"type": "Namespace", "link": "App/Services.html", "name": "App\\Services", "doc": "Namespace App\\Services"},{"type": "Namespace", "link": "App/Transformers.html", "name": "App\\Transformers", "doc": "Namespace App\\Transformers"},
            {"type": "Interface", "fromName": "App\\Contracts", "fromLink": "App/Contracts.html", "link": "App/Contracts/DocumentManagementServiceInterface.html", "name": "App\\Contracts\\DocumentManagementServiceInterface", "doc": "&quot;Interface DocumentManagementServiceInterface&quot;"},
                                                        {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_get", "name": "App\\Contracts\\DocumentManagementServiceInterface::get", "doc": "&quot;Obt\u00e9m metadados de um Document&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_delete", "name": "App\\Contracts\\DocumentManagementServiceInterface::delete", "doc": "&quot;Exclui permanentemente um Document do disco e remove os registros de metadados.&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_getContents", "name": "App\\Contracts\\DocumentManagementServiceInterface::getContents", "doc": "&quot;Realiza a leitura e retorna o conte\u00fado de um Document no disco&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_getUrl", "name": "App\\Contracts\\DocumentManagementServiceInterface::getUrl", "doc": "&quot;Retorna a url web de um Document&quot;"},
            
            {"type": "Interface", "fromName": "App\\Contracts", "fromLink": "App/Contracts.html", "link": "App/Contracts/DocumentRepositoryInterface.html", "name": "App\\Contracts\\DocumentRepositoryInterface", "doc": "&quot;Interface DocumentRepositoryInterface&quot;"},
                                                        {"type": "Method", "fromName": "App\\Contracts\\DocumentRepositoryInterface", "fromLink": "App/Contracts/DocumentRepositoryInterface.html", "link": "App/Contracts/DocumentRepositoryInterface.html#method_save", "name": "App\\Contracts\\DocumentRepositoryInterface::save", "doc": "&quot;Salva os dados de um documento&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentRepositoryInterface", "fromLink": "App/Contracts/DocumentRepositoryInterface.html", "link": "App/Contracts/DocumentRepositoryInterface.html#method_find", "name": "App\\Contracts\\DocumentRepositoryInterface::find", "doc": "&quot;Recebe um array assossiativo mapeando os crit\u00e9rios para a busca de um \u00fanico\nobjeto&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentRepositoryInterface", "fromLink": "App/Contracts/DocumentRepositoryInterface.html", "link": "App/Contracts/DocumentRepositoryInterface.html#method_delete", "name": "App\\Contracts\\DocumentRepositoryInterface::delete", "doc": "&quot;Remove permanentemente um registro&quot;"},
            
            {"type": "Interface", "fromName": "App\\Contracts", "fromLink": "App/Contracts.html", "link": "App/Contracts/UploadServiceInterface.html", "name": "App\\Contracts\\UploadServiceInterface", "doc": "&quot;Class Uploader&quot;"},
                                                        {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_setResource", "name": "App\\Contracts\\UploadServiceInterface::setResource", "doc": "&quot;Determina qual ser\u00e1 o subdiret\u00f3rio utilizado para armazenar o arquivo&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_handle", "name": "App\\Contracts\\UploadServiceInterface::handle", "doc": "&quot;Recebe os dados e determina delega a a\u00e7\u00e3o para o m\u00e9todo que gerencia um ou para o m\u00e9todo\nque gerencia v\u00e1rios arquivos, dependendo do tipo de dados recebidos.&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_handleMultipleFiles", "name": "App\\Contracts\\UploadServiceInterface::handleMultipleFiles", "doc": "&quot;Trata o caso do recebimento de v\u00e1rios arquivos no mesmo Request&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_handleSingleFile", "name": "App\\Contracts\\UploadServiceInterface::handleSingleFile", "doc": "&quot;Realiza o upload de um \u00fanico arquivo e retorna um objeto Document com as informa\u00e7\u00f5es\nsobre o arquivo que foi recebido&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_addFileToList", "name": "App\\Contracts\\UploadServiceInterface::addFileToList", "doc": "&quot;Adiciona na lista de arquivos desta inst\u00e2ncia um Document representando um arquivo&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_getUploadedFilesList", "name": "App\\Contracts\\UploadServiceInterface::getUploadedFilesList", "doc": "&quot;Retorna a lista de de objetos Document desta inst\u00e2ncia&quot;"},
            
            
            {"type": "Class", "fromName": "App\\Console", "fromLink": "App/Console.html", "link": "App/Console/Kernel.html", "name": "App\\Console\\Kernel", "doc": "&quot;Class Kernel&quot;"},
                    
            {"type": "Class", "fromName": "App\\Contracts", "fromLink": "App/Contracts.html", "link": "App/Contracts/DocumentManagementServiceInterface.html", "name": "App\\Contracts\\DocumentManagementServiceInterface", "doc": "&quot;Interface DocumentManagementServiceInterface&quot;"},
                                                        {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_get", "name": "App\\Contracts\\DocumentManagementServiceInterface::get", "doc": "&quot;Obt\u00e9m metadados de um Document&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_delete", "name": "App\\Contracts\\DocumentManagementServiceInterface::delete", "doc": "&quot;Exclui permanentemente um Document do disco e remove os registros de metadados.&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_getContents", "name": "App\\Contracts\\DocumentManagementServiceInterface::getContents", "doc": "&quot;Realiza a leitura e retorna o conte\u00fado de um Document no disco&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentManagementServiceInterface", "fromLink": "App/Contracts/DocumentManagementServiceInterface.html", "link": "App/Contracts/DocumentManagementServiceInterface.html#method_getUrl", "name": "App\\Contracts\\DocumentManagementServiceInterface::getUrl", "doc": "&quot;Retorna a url web de um Document&quot;"},
            
            {"type": "Class", "fromName": "App\\Contracts", "fromLink": "App/Contracts.html", "link": "App/Contracts/DocumentRepositoryInterface.html", "name": "App\\Contracts\\DocumentRepositoryInterface", "doc": "&quot;Interface DocumentRepositoryInterface&quot;"},
                                                        {"type": "Method", "fromName": "App\\Contracts\\DocumentRepositoryInterface", "fromLink": "App/Contracts/DocumentRepositoryInterface.html", "link": "App/Contracts/DocumentRepositoryInterface.html#method_save", "name": "App\\Contracts\\DocumentRepositoryInterface::save", "doc": "&quot;Salva os dados de um documento&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentRepositoryInterface", "fromLink": "App/Contracts/DocumentRepositoryInterface.html", "link": "App/Contracts/DocumentRepositoryInterface.html#method_find", "name": "App\\Contracts\\DocumentRepositoryInterface::find", "doc": "&quot;Recebe um array assossiativo mapeando os crit\u00e9rios para a busca de um \u00fanico\nobjeto&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\DocumentRepositoryInterface", "fromLink": "App/Contracts/DocumentRepositoryInterface.html", "link": "App/Contracts/DocumentRepositoryInterface.html#method_delete", "name": "App\\Contracts\\DocumentRepositoryInterface::delete", "doc": "&quot;Remove permanentemente um registro&quot;"},
            
            {"type": "Class", "fromName": "App\\Contracts", "fromLink": "App/Contracts.html", "link": "App/Contracts/UploadServiceInterface.html", "name": "App\\Contracts\\UploadServiceInterface", "doc": "&quot;Class Uploader&quot;"},
                                                        {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_setResource", "name": "App\\Contracts\\UploadServiceInterface::setResource", "doc": "&quot;Determina qual ser\u00e1 o subdiret\u00f3rio utilizado para armazenar o arquivo&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_handle", "name": "App\\Contracts\\UploadServiceInterface::handle", "doc": "&quot;Recebe os dados e determina delega a a\u00e7\u00e3o para o m\u00e9todo que gerencia um ou para o m\u00e9todo\nque gerencia v\u00e1rios arquivos, dependendo do tipo de dados recebidos.&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_handleMultipleFiles", "name": "App\\Contracts\\UploadServiceInterface::handleMultipleFiles", "doc": "&quot;Trata o caso do recebimento de v\u00e1rios arquivos no mesmo Request&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_handleSingleFile", "name": "App\\Contracts\\UploadServiceInterface::handleSingleFile", "doc": "&quot;Realiza o upload de um \u00fanico arquivo e retorna um objeto Document com as informa\u00e7\u00f5es\nsobre o arquivo que foi recebido&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_addFileToList", "name": "App\\Contracts\\UploadServiceInterface::addFileToList", "doc": "&quot;Adiciona na lista de arquivos desta inst\u00e2ncia um Document representando um arquivo&quot;"},
                    {"type": "Method", "fromName": "App\\Contracts\\UploadServiceInterface", "fromLink": "App/Contracts/UploadServiceInterface.html", "link": "App/Contracts/UploadServiceInterface.html#method_getUploadedFilesList", "name": "App\\Contracts\\UploadServiceInterface::getUploadedFilesList", "doc": "&quot;Retorna a lista de de objetos Document desta inst\u00e2ncia&quot;"},
            
            {"type": "Class", "fromName": "App\\Events", "fromLink": "App/Events.html", "link": "App/Events/Event.html", "name": "App\\Events\\Event", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Events", "fromLink": "App/Events.html", "link": "App/Events/ExampleEvent.html", "name": "App\\Events\\ExampleEvent", "doc": "&quot;Class ExampleEvent&quot;"},
                                                        {"type": "Method", "fromName": "App\\Events\\ExampleEvent", "fromLink": "App/Events/ExampleEvent.html", "link": "App/Events/ExampleEvent.html#method___construct", "name": "App\\Events\\ExampleEvent::__construct", "doc": "&quot;Create a new event instance.&quot;"},
            
            {"type": "Class", "fromName": "App\\Exceptions", "fromLink": "App/Exceptions.html", "link": "App/Exceptions/DocumentNotFoundException.html", "name": "App\\Exceptions\\DocumentNotFoundException", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Exceptions", "fromLink": "App/Exceptions.html", "link": "App/Exceptions/DocumentTooLargeException.html", "name": "App\\Exceptions\\DocumentTooLargeException", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Exceptions", "fromLink": "App/Exceptions.html", "link": "App/Exceptions/Handler.html", "name": "App\\Exceptions\\Handler", "doc": "&quot;Class Handler&quot;"},
                                                        {"type": "Method", "fromName": "App\\Exceptions\\Handler", "fromLink": "App/Exceptions/Handler.html", "link": "App/Exceptions/Handler.html#method_report", "name": "App\\Exceptions\\Handler::report", "doc": "&quot;Report or log an exception.&quot;"},
                    {"type": "Method", "fromName": "App\\Exceptions\\Handler", "fromLink": "App/Exceptions/Handler.html", "link": "App/Exceptions/Handler.html#method_render", "name": "App\\Exceptions\\Handler::render", "doc": "&quot;Render an exception into an HTTP response.&quot;"},
            
            {"type": "Class", "fromName": "App\\Exceptions", "fromLink": "App/Exceptions.html", "link": "App/Exceptions/MimeTypeNotAllowedException.html", "name": "App\\Exceptions\\MimeTypeNotAllowedException", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Exceptions", "fromLink": "App/Exceptions.html", "link": "App/Exceptions/MimeTypeNotWhitelistedException.html", "name": "App\\Exceptions\\MimeTypeNotWhitelistedException", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/Controller.html", "name": "App\\Http\\Controllers\\Controller", "doc": "&quot;Class Controller&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Controller", "fromLink": "App/Http/Controllers/Controller.html", "link": "App/Http/Controllers/Controller.html#method_buildItemResponse", "name": "App\\Http\\Controllers\\Controller::buildItemResponse", "doc": "&quot;Create the response for an item.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\Controller", "fromLink": "App/Http/Controllers/Controller.html", "link": "App/Http/Controllers/Controller.html#method_buildCollectionResponse", "name": "App\\Http\\Controllers\\Controller::buildCollectionResponse", "doc": "&quot;Create the response for a collection.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\Controller", "fromLink": "App/Http/Controllers/Controller.html", "link": "App/Http/Controllers/Controller.html#method_buildResourceResponse", "name": "App\\Http\\Controllers\\Controller::buildResourceResponse", "doc": "&quot;Create the response for a resource.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/DocumentsController.html", "name": "App\\Http\\Controllers\\DocumentsController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\DocumentsController", "fromLink": "App/Http/Controllers/DocumentsController.html", "link": "App/Http/Controllers/DocumentsController.html#method___construct", "name": "App\\Http\\Controllers\\DocumentsController::__construct", "doc": "&quot;FilesController constructor.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DocumentsController", "fromLink": "App/Http/Controllers/DocumentsController.html", "link": "App/Http/Controllers/DocumentsController.html#method_postUpload", "name": "App\\Http\\Controllers\\DocumentsController::postUpload", "doc": "&quot;Upload de arquivos&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DocumentsController", "fromLink": "App/Http/Controllers/DocumentsController.html", "link": "App/Http/Controllers/DocumentsController.html#method_showFile", "name": "App\\Http\\Controllers\\DocumentsController::showFile", "doc": "&quot;Exibe um arquivo&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DocumentsController", "fromLink": "App/Http/Controllers/DocumentsController.html", "link": "App/Http/Controllers/DocumentsController.html#method_deleteFile", "name": "App\\Http\\Controllers\\DocumentsController::deleteFile", "doc": "&quot;Remove permamentemente um documento&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DocumentsController", "fromLink": "App/Http/Controllers/DocumentsController.html", "link": "App/Http/Controllers/DocumentsController.html#method_transformImage", "name": "App\\Http\\Controllers\\DocumentsController::transformImage", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/Authenticate.html", "name": "App\\Http\\Middleware\\Authenticate", "doc": "&quot;Class Authenticate&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\Authenticate", "fromLink": "App/Http/Middleware/Authenticate.html", "link": "App/Http/Middleware/Authenticate.html#method___construct", "name": "App\\Http\\Middleware\\Authenticate::__construct", "doc": "&quot;Create a new middleware instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\Authenticate", "fromLink": "App/Http/Middleware/Authenticate.html", "link": "App/Http/Middleware/Authenticate.html#method_handle", "name": "App\\Http\\Middleware\\Authenticate::handle", "doc": "&quot;Handle an incoming request.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/SimpleApiAuth.html", "name": "App\\Http\\Middleware\\SimpleApiAuth", "doc": "&quot;Class ExampleMiddleware&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\SimpleApiAuth", "fromLink": "App/Http/Middleware/SimpleApiAuth.html", "link": "App/Http/Middleware/SimpleApiAuth.html#method_handle", "name": "App\\Http\\Middleware\\SimpleApiAuth::handle", "doc": "&quot;Handle an incoming request.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/ThrottleRequests.html", "name": "App\\Http\\Middleware\\ThrottleRequests", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\ThrottleRequests", "fromLink": "App/Http/Middleware/ThrottleRequests.html", "link": "App/Http/Middleware/ThrottleRequests.html#method___construct", "name": "App\\Http\\Middleware\\ThrottleRequests::__construct", "doc": "&quot;Create a new request throttler.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\ThrottleRequests", "fromLink": "App/Http/Middleware/ThrottleRequests.html", "link": "App/Http/Middleware/ThrottleRequests.html#method_handle", "name": "App\\Http\\Middleware\\ThrottleRequests::handle", "doc": "&quot;Handle an incoming request.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\ThrottleRequests", "fromLink": "App/Http/Middleware/ThrottleRequests.html", "link": "App/Http/Middleware/ThrottleRequests.html#method_resolveRequestSignature", "name": "App\\Http\\Middleware\\ThrottleRequests::resolveRequestSignature", "doc": "&quot;Resolve request signature.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\ThrottleRequests", "fromLink": "App/Http/Middleware/ThrottleRequests.html", "link": "App/Http/Middleware/ThrottleRequests.html#method_buildResponse", "name": "App\\Http\\Middleware\\ThrottleRequests::buildResponse", "doc": "&quot;Create a &#039;too many attempts&#039; response.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\ThrottleRequests", "fromLink": "App/Http/Middleware/ThrottleRequests.html", "link": "App/Http/Middleware/ThrottleRequests.html#method_addHeaders", "name": "App\\Http\\Middleware\\ThrottleRequests::addHeaders", "doc": "&quot;Add the limit header information to the given response.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\ThrottleRequests", "fromLink": "App/Http/Middleware/ThrottleRequests.html", "link": "App/Http/Middleware/ThrottleRequests.html#method_calculateRemainingAttempts", "name": "App\\Http\\Middleware\\ThrottleRequests::calculateRemainingAttempts", "doc": "&quot;Calculate the number of remaining attempts.&quot;"},
            
            {"type": "Class", "fromName": "App\\Jobs", "fromLink": "App/Jobs.html", "link": "App/Jobs/ExampleJob.html", "name": "App\\Jobs\\ExampleJob", "doc": "&quot;Class ExampleJob&quot;"},
                                                        {"type": "Method", "fromName": "App\\Jobs\\ExampleJob", "fromLink": "App/Jobs/ExampleJob.html", "link": "App/Jobs/ExampleJob.html#method___construct", "name": "App\\Jobs\\ExampleJob::__construct", "doc": "&quot;Create a new job instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Jobs\\ExampleJob", "fromLink": "App/Jobs/ExampleJob.html", "link": "App/Jobs/ExampleJob.html#method_handle", "name": "App\\Jobs\\ExampleJob::handle", "doc": "&quot;Execute the job.&quot;"},
            
            {"type": "Class", "fromName": "App\\Jobs", "fromLink": "App/Jobs.html", "link": "App/Jobs/Job.html", "name": "App\\Jobs\\Job", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Libraries", "fromLink": "App/Libraries.html", "link": "App/Libraries/Formatter.html", "name": "App\\Libraries\\Formatter", "doc": "&quot;Class Formatter&quot;"},
                                                        {"type": "Method", "fromName": "App\\Libraries\\Formatter", "fromLink": "App/Libraries/Formatter.html", "link": "App/Libraries/Formatter.html#method_bytes", "name": "App\\Libraries\\Formatter::bytes", "doc": "&quot;Converte um valor recebido em bytes para uma string formatada\nadicionando um sufixo de unidade&quot;"},
            
            {"type": "Class", "fromName": "App\\Models", "fromLink": "App/Models.html", "link": "App/Models/Document.html", "name": "App\\Models\\Document", "doc": "&quot;Class Document&quot;"},
                                                        {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method___construct", "name": "App\\Models\\Document::__construct", "doc": "&quot;Document constructor.&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getId", "name": "App\\Models\\Document::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setId", "name": "App\\Models\\Document::setId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getOriginalName", "name": "App\\Models\\Document::getOriginalName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setOriginalName", "name": "App\\Models\\Document::setOriginalName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getExtension", "name": "App\\Models\\Document::getExtension", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setExtension", "name": "App\\Models\\Document::setExtension", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getMimeType", "name": "App\\Models\\Document::getMimeType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setMimeType", "name": "App\\Models\\Document::setMimeType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getPath", "name": "App\\Models\\Document::getPath", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setPath", "name": "App\\Models\\Document::setPath", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getUrl", "name": "App\\Models\\Document::getUrl", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setUrl", "name": "App\\Models\\Document::setUrl", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getStorageDisk", "name": "App\\Models\\Document::getStorageDisk", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setStorageDisk", "name": "App\\Models\\Document::setStorageDisk", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getHash", "name": "App\\Models\\Document::getHash", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setHash", "name": "App\\Models\\Document::setHash", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getContents", "name": "App\\Models\\Document::getContents", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setContents", "name": "App\\Models\\Document::setContents", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_toArray", "name": "App\\Models\\Document::toArray", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setDateTime", "name": "App\\Models\\Document::setDateTime", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getDateTime", "name": "App\\Models\\Document::getDateTime", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_getSize", "name": "App\\Models\\Document::getSize", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Models\\Document", "fromLink": "App/Models/Document.html", "link": "App/Models/Document.html#method_setSize", "name": "App\\Models\\Document::setSize", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/AppServiceProvider.html", "name": "App\\Providers\\AppServiceProvider", "doc": "&quot;Class AppServiceProvider&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\AppServiceProvider", "fromLink": "App/Providers/AppServiceProvider.html", "link": "App/Providers/AppServiceProvider.html#method_register", "name": "App\\Providers\\AppServiceProvider::register", "doc": "&quot;Register any application services.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/AuthServiceProvider.html", "name": "App\\Providers\\AuthServiceProvider", "doc": "&quot;Class AuthServiceProvider&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\AuthServiceProvider", "fromLink": "App/Providers/AuthServiceProvider.html", "link": "App/Providers/AuthServiceProvider.html#method_register", "name": "App\\Providers\\AuthServiceProvider::register", "doc": "&quot;Register any application services.&quot;"},
                    {"type": "Method", "fromName": "App\\Providers\\AuthServiceProvider", "fromLink": "App/Providers/AuthServiceProvider.html", "link": "App/Providers/AuthServiceProvider.html#method_boot", "name": "App\\Providers\\AuthServiceProvider::boot", "doc": "&quot;Boot the authentication services for the application.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/EventServiceProvider.html", "name": "App\\Providers\\EventServiceProvider", "doc": "&quot;Class EventServiceProvider&quot;"},
                    
            {"type": "Class", "fromName": "App\\Repositories", "fromLink": "App/Repositories.html", "link": "App/Repositories/DocumentRepositoryMongo.html", "name": "App\\Repositories\\DocumentRepositoryMongo", "doc": "&quot;Class DocumentRepositoryMongo\ngerencia os documentos utilizando persist\u00eancia com MongoDB&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryMongo", "fromLink": "App/Repositories/DocumentRepositoryMongo.html", "link": "App/Repositories/DocumentRepositoryMongo.html#method___construct", "name": "App\\Repositories\\DocumentRepositoryMongo::__construct", "doc": "&quot;DocumentRepositoryMongo constructor.&quot;"},
                    {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryMongo", "fromLink": "App/Repositories/DocumentRepositoryMongo.html", "link": "App/Repositories/DocumentRepositoryMongo.html#method_save", "name": "App\\Repositories\\DocumentRepositoryMongo::save", "doc": "&quot;Salva os dados de um documento&quot;"},
                    {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryMongo", "fromLink": "App/Repositories/DocumentRepositoryMongo.html", "link": "App/Repositories/DocumentRepositoryMongo.html#method_find", "name": "App\\Repositories\\DocumentRepositoryMongo::find", "doc": "&quot;Recebe um array assossiativo mapeando os crit\u00e9rios para a busca de um \u00fanico\nobjeto&quot;"},
                    {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryMongo", "fromLink": "App/Repositories/DocumentRepositoryMongo.html", "link": "App/Repositories/DocumentRepositoryMongo.html#method_delete", "name": "App\\Repositories\\DocumentRepositoryMongo::delete", "doc": "&quot;Remove permanentemente um registro&quot;"},
            
            {"type": "Class", "fromName": "App\\Repositories", "fromLink": "App/Repositories.html", "link": "App/Repositories/DocumentRepositoryRedis.html", "name": "App\\Repositories\\DocumentRepositoryRedis", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryRedis", "fromLink": "App/Repositories/DocumentRepositoryRedis.html", "link": "App/Repositories/DocumentRepositoryRedis.html#method___construct", "name": "App\\Repositories\\DocumentRepositoryRedis::__construct", "doc": "&quot;DocumentRepositoryRedis constructor.&quot;"},
                    {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryRedis", "fromLink": "App/Repositories/DocumentRepositoryRedis.html", "link": "App/Repositories/DocumentRepositoryRedis.html#method_save", "name": "App\\Repositories\\DocumentRepositoryRedis::save", "doc": "&quot;Salva os dados de um documento&quot;"},
                    {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryRedis", "fromLink": "App/Repositories/DocumentRepositoryRedis.html", "link": "App/Repositories/DocumentRepositoryRedis.html#method_find", "name": "App\\Repositories\\DocumentRepositoryRedis::find", "doc": "&quot;Recebe um array assossiativo mapeando os crit\u00e9rios para a busca de um \u00fanico\nobjeto&quot;"},
                    {"type": "Method", "fromName": "App\\Repositories\\DocumentRepositoryRedis", "fromLink": "App/Repositories/DocumentRepositoryRedis.html", "link": "App/Repositories/DocumentRepositoryRedis.html#method_delete", "name": "App\\Repositories\\DocumentRepositoryRedis::delete", "doc": "&quot;Remove permanentemente um registro&quot;"},
            
            {"type": "Class", "fromName": "App\\Services", "fromLink": "App/Services.html", "link": "App/Services/DocumentManagementService.html", "name": "App\\Services\\DocumentManagementService", "doc": "&quot;Class DocumentManagementService&quot;"},
                                                        {"type": "Method", "fromName": "App\\Services\\DocumentManagementService", "fromLink": "App/Services/DocumentManagementService.html", "link": "App/Services/DocumentManagementService.html#method___construct", "name": "App\\Services\\DocumentManagementService::__construct", "doc": "&quot;FileManager constructor.&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\DocumentManagementService", "fromLink": "App/Services/DocumentManagementService.html", "link": "App/Services/DocumentManagementService.html#method_get", "name": "App\\Services\\DocumentManagementService::get", "doc": "&quot;Obt\u00e9m metadados de um Document&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\DocumentManagementService", "fromLink": "App/Services/DocumentManagementService.html", "link": "App/Services/DocumentManagementService.html#method_delete", "name": "App\\Services\\DocumentManagementService::delete", "doc": "&quot;Exclui permanentemente um Document do disco e remove os registros de metadados.&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\DocumentManagementService", "fromLink": "App/Services/DocumentManagementService.html", "link": "App/Services/DocumentManagementService.html#method_getContents", "name": "App\\Services\\DocumentManagementService::getContents", "doc": "&quot;Realiza a leitura e retorna o conte\u00fado de um Document no disco&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\DocumentManagementService", "fromLink": "App/Services/DocumentManagementService.html", "link": "App/Services/DocumentManagementService.html#method_getUrl", "name": "App\\Services\\DocumentManagementService::getUrl", "doc": "&quot;Retorna a url web de um Document&quot;"},
            
            {"type": "Class", "fromName": "App\\Services", "fromLink": "App/Services.html", "link": "App/Services/UploadService.html", "name": "App\\Services\\UploadService", "doc": "&quot;Class Uploader&quot;"},
                                                        {"type": "Method", "fromName": "App\\Services\\UploadService", "fromLink": "App/Services/UploadService.html", "link": "App/Services/UploadService.html#method___construct", "name": "App\\Services\\UploadService::__construct", "doc": "&quot;Uploader constructor.&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadService", "fromLink": "App/Services/UploadService.html", "link": "App/Services/UploadService.html#method_setResource", "name": "App\\Services\\UploadService::setResource", "doc": "&quot;Determina qual ser\u00e1 o subdiret\u00f3rio utilizado para armazenar o arquivo&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadService", "fromLink": "App/Services/UploadService.html", "link": "App/Services/UploadService.html#method_handle", "name": "App\\Services\\UploadService::handle", "doc": "&quot;Recebe os dados e determina delega a a\u00e7\u00e3o para o m\u00e9todo que gerencia um ou para o m\u00e9todo\nque gerencia v\u00e1rios arquivos, dependendo do tipo de dados recebidos.&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadService", "fromLink": "App/Services/UploadService.html", "link": "App/Services/UploadService.html#method_handleMultipleFiles", "name": "App\\Services\\UploadService::handleMultipleFiles", "doc": "&quot;Trata o caso do recebimento de v\u00e1rios arquivos no mesmo Request&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadService", "fromLink": "App/Services/UploadService.html", "link": "App/Services/UploadService.html#method_handleSingleFile", "name": "App\\Services\\UploadService::handleSingleFile", "doc": "&quot;Realiza o upload de um \u00fanico arquivo e retorna um objeto Document com as informa\u00e7\u00f5es\nsobre o arquivo que foi recebido&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadService", "fromLink": "App/Services/UploadService.html", "link": "App/Services/UploadService.html#method_addFileToList", "name": "App\\Services\\UploadService::addFileToList", "doc": "&quot;Adiciona na lista de arquivos desta inst\u00e2ncia um Document representando um arquivo&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadService", "fromLink": "App/Services/UploadService.html", "link": "App/Services/UploadService.html#method_getUploadedFilesList", "name": "App\\Services\\UploadService::getUploadedFilesList", "doc": "&quot;Retorna a lista de de objetos Document desta inst\u00e2ncia&quot;"},
            
            {"type": "Class", "fromName": "App\\Services", "fromLink": "App/Services.html", "link": "App/Services/UploadValidationService.html", "name": "App\\Services\\UploadValidationService", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Services\\UploadValidationService", "fromLink": "App/Services/UploadValidationService.html", "link": "App/Services/UploadValidationService.html#method_validateMimeType", "name": "App\\Services\\UploadValidationService::validateMimeType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadValidationService", "fromLink": "App/Services/UploadValidationService.html", "link": "App/Services/UploadValidationService.html#method_validateImage", "name": "App\\Services\\UploadValidationService::validateImage", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadValidationService", "fromLink": "App/Services/UploadValidationService.html", "link": "App/Services/UploadValidationService.html#method_getAllowedMimeTypesByResource", "name": "App\\Services\\UploadValidationService::getAllowedMimeTypesByResource", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadValidationService", "fromLink": "App/Services/UploadValidationService.html", "link": "App/Services/UploadValidationService.html#method_validateResourceUpload", "name": "App\\Services\\UploadValidationService::validateResourceUpload", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Services\\UploadValidationService", "fromLink": "App/Services/UploadValidationService.html", "link": "App/Services/UploadValidationService.html#method_validateWhitelisted", "name": "App\\Services\\UploadValidationService::validateWhitelisted", "doc": "&quot;Verifica se um arquivo de upload est\u00e1 dentro da lista de mime types permitidos no arquivo de configura\u00e7\u00e3o.&quot;"},
            
            {"type": "Class", "fromName": "App\\Transformers", "fromLink": "App/Transformers.html", "link": "App/Transformers/DocumentTransformer.html", "name": "App\\Transformers\\DocumentTransformer", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Transformers\\DocumentTransformer", "fromLink": "App/Transformers/DocumentTransformer.html", "link": "App/Transformers/DocumentTransformer.html#method_transform", "name": "App\\Transformers\\DocumentTransformer::transform", "doc": "&quot;&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


