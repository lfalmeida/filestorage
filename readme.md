# FileStorage 

O objetivo deste projeto é prover um serviço de armazenamento e recuperação de arquivos, de maneira transparente para as aplicações cliente.   

### Os principais pacotes utilizados são:    
- [Lumen](https://lumen.laravel.com) micro framework, baseado no Laravel.
- [Flysystem](https://flysystem.thephpleague.com) para abstração do gerenciamento de arquivos.
- [Glide](http://glide.thephpleague.com) para manupulação de imagens via HTTP.

### Recursos (v0.7.0)
* Upload de múltiplos arquivos por request.
* Suporte para armazenamento de maneira transparente no [S3](https://aws.amazon.com/s3/).
* Armazenamento local de arquivos.
* Manipulação de imagens via query string.
* Validação de mime type, e tamanho de arquivos configuráveis.
* Correção automática de orientação para arquivos jpg utilizando exif.
* Leitura e armazenamento de informações de geolocalização presentes nas fotos.
* Tratamento para upload de imagens base64.
* Integrando report de exceptions com Sentry.

### Recursos em desenvolvimento (v0.8.0)
* Integração com API externa de análise do conteúdo das imagens.

#### Documentação
A documentação das classes e da API está disponível dentro do projeto, no diretório public/.
- Para atualizar a documentação gerada utilizando o Sami:
 ``$ composer docs``
#### Instalação
Após clonar o projeto:   
``$ composer install``   
Copiar o arquivo ```.env.example``` para ```.env``` e atualizar os dados deste arquivo conforme o necessário. 

#### Utilização
O envio de arquivos é via POST, suportando um ou mais arquivos por request.        
Exemplo de envio via CURL:        
```curl http://<project_root>/<resource> -H 'authorization: <apikey>' -F file=@/file/path/upload.jpg```   
- ```<resource>``` indica a entidade que o arquivo é relacionado.  
- ```<apikey>``` é configurada no arquivo .env, é obrigatória e deve ser enviada no header.

[Mais informações](https://bitbucket.org/lfalmeida/filestorage/wiki/Home)

##### Dependências
PHP >= 7.0   
Utilizando MongoDB:
* Extensão do PHP: mongodb
* Servidor MongoDB

Utilizando Redis:   
* Servidor Redis
* Paconte predis/predis

 
