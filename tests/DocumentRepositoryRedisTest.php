<?php

use App\Repositories\DocumentRepositoryRedis;
use Mockery as m;

class DocumentRepositoryRedisTest extends TestCase
{
    protected $repo;

    public function setUp()
    {
        parent::setUp();

    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     *
     */
    public function testShouldSaveADocument()
    {
        // 1 - arrange
        $mockRedisClient = m::mock(Predis\Client::class);
        $mockRedisClient->shouldReceive('set')
            ->once()
            ->andReturn();

        $this->app->instance('RedisClient', $mockRedisClient);

        $repo = new DocumentRepositoryRedis();
        $entityData = [
            "id" => "1493637470.5907195e503557.59064718",
            "originalName" => "fotinha.jpg",
            "extension" => "jpg",
            "mimeType" => "text/markdown",
            "path" => "tests/1493637470.5907195e503557.59064718.jpg",
            "storageDisk" => "local",
            "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
        ];

        // 2 - act
        $result = $repo->save($entityData);

        // 3 - assert
        $this->assertTrue(intval($result) > 0);


    }


    /**
     *
     */
    public function testShouldFindAExistingDocument()
    {
        // arrange
        $mockRedisClient = m::mock(Predis\Client::class);
        $mockRedisClient->shouldReceive('get')
            ->once()
            ->andReturn(json_encode([
                "id" => "1493637470.5907195e503557.59064718",
                "originalName" => "fotinha.jpg",
                "extension" => "jpg",
                "mimeType" => "image/jpeg",
                "path" => "avatars/1493637470.5907195e503557.59064718.jpg",
                "storageDisk" => "local",
                "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
            ]));

        $this->app->instance('RedisClient', $mockRedisClient);
        $path = 'avatars/1493637470.5907195e503557.59064718.jpg';
        // 2 - act
        $repo = new DocumentRepositoryRedis();
        $doc = $repo->find(['path' => $path]);

        // 3 - assert
        $this->assertEquals($doc->getPath(), $path);

    }

    /**
     * @expectedException \App\Exceptions\DocumentNotFoundException
     */
    public function testShouldThrowExceptionWhenTryingToFindANonExistingDocument()
    {
        // arrange
        $mockRedisClient = m::mock(Predis\Client::class);
        $mockRedisClient->shouldReceive('get')
            ->once()
            ->andReturn(null);

        $this->app->instance('RedisClient', $mockRedisClient);
        $path = 'invalid/not-exists.jpg';

        // 2 - act
        $repo = new DocumentRepositoryRedis();
        $repo->find(['path' => $path]);
        // 3 - assert
        // Espera a exception declarada no dockblock anotation deste método
    }

    /**
     *
     */
    public function testShouldDeleteAExistingDocument()
    {
        // arrange
        $mockRedisClient = m::mock(Predis\Client::class);
        $mockRedisClient->shouldReceive('del')
            ->andReturn(true);

        $this->app->instance('RedisClient', $mockRedisClient);
        // act
        $repo = new DocumentRepositoryRedis();
        $result = $repo->delete(['path' => 'tests/1493637470.5907195e503557.59064718.md']);
        $this->assertTrue($result);
    }
}
