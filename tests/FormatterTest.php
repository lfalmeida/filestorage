<?php

namespace tests;

use App\Libraries\Formatter;

class FormatterTest extends \PHPUnit_Framework_TestCase
{

    public function testShoudConvertBytesToMegaBytes()
    {
        $sizeInBytes = 24962496;
        $formatter = new Formatter();
        $result = $formatter->bytes($sizeInBytes, $precision = 2);
        $this->assertEquals('23.81 MB', $result);
    }
}
