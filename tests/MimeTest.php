<?php


namespace App\Libraries;

use TestCase;

class MimeTest extends TestCase
{
    /**
     * Verifica a leitura da base de mimes json ao instanciar o objeto
     */
    public function testShouldLoadMimeStaticLocalDatabase()
    {
        $mime = new Mime();
        $this->assertNotEmpty($mime->getTypes());
    }

    /**
     * Verifica o retorno do mime type para determinadas extensões
     */
    public function testShouldGetMimeTypeForExtension()
    {
        $mime = new Mime();
        // Testando o retorno para alguns tipos comuns
        // imagens
        $this->assertEquals('image/png', $mime->getMimeTyepForExtension('png'));
        $this->assertEquals('image/jpeg', $mime->getMimeTyepForExtension('jpg'));
        $this->assertEquals('image/gif', $mime->getMimeTyepForExtension('gif'));
        // pdf
        $this->assertEquals('application/pdf', $mime->getMimeTyepForExtension('pdf'));

        // arquivos do office
        $this->assertEquals('application/vnd.openxmlformats-officedocument.presentationml.presentation',
            $mime->getMimeTyepForExtension('pptx'));

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $mime->getMimeTyepForExtension('xlsx'));

        $this->assertEquals('application/vnd.ms-excel',
            $mime->getMimeTyepForExtension('xls'));
    }

    /**
     *
     */
    public function testShouldDetermineIfAnTypeIsKnown()
    {
        $mime = new Mime();
        $this->assertTrue($mime->hasType('application/pdf'));
    }

    /**
     *
     */
    public function testShouldDetermineIfAnTypeIsUnknown()
    {
        $mime = new Mime();
        $this->assertFalse($mime->hasType('stream/invalid'));
    }

    /**
     *
     */
    public function testShouldDetermineIfAnExtensionIsKnown()
    {
        $mime = new Mime();
        $this->assertTrue($mime->hasExtension('pdf'));
    }

    /**
     * Testa o retorno para uma extensão desconhecida
     */
    public function testShouldDetermineIfAnExtensionIsUnknown()
    {
        $mime = new Mime();
        $this->assertFalse($mime->hasExtension('xpto'));
    }

    /**
     * Testa o retorno de uma extensão de acordo com o mime type fornecido
     */
    public function testShouldReturnTheExtensionAssossiatedWhithAGivenMimeType()
    {
        $mime = new Mime();
        $extension = $mime->getExtensionForType('application/pdf');
        $this->assertEquals('pdf', $extension);
    }
}
