<?php

use App\Contracts\DocumentRepositoryInterface;
use App\Repositories\DocumentRepositoryMongo;
use App\Services\DocumentManagementService;
use App\Services\GoogleVisionClient;

use Illuminate\Http\UploadedFile;
use Mockery as m;

class GoogleVisionServiceTest extends TestCase
{
    protected $responseSafe = '{
                  "responses": [
                    {
                      "safeSearchAnnotation": {
                        "adult": "VERY_UNLIKELY",
                        "spoof": "VERY_UNLIKELY",
                        "medical": "UNLIKELY",
                        "violence": "UNLIKELY"
                      }
                    }
                  ]
                }';

    protected $responseUnsafe = '{
                  "responses": [
                    {
                      "safeSearchAnnotation": {
                        "adult": "VERY_LIKELY",
                        "spoof": "LIKELY",
                        "medical": "UNLIKELY",
                        "violence": "UNLIKELY"
                      }
                    }
                  ]
                }';

    public function setUp()
    {
        parent::setUp();
        $mockMongoClient = m::mock(MongoDB\Client::class);
        $mockMongoClient->db = (object)['db'];
        $mockMongoClient->db->docs = m::mock(MongoDB\Collection::class);

        $mockResult = m::mock(stdClass::class);
        $mockResult->shouldReceive('getArrayCopy')
            ->andReturn([
                "id" => "1493637470.5907195e503557.59064718",
                "originalName" => "readme.md",
                "extension" => "md",
                "mimeType" => "text/markdown",
                "path" => "tests/1493637470.5907195e503557.59064718.md",
                "storageDisk" => "local",
                "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
            ]);

        $mockMongoClient->db->docs->shouldReceive('findOne')
            ->andReturn($mockResult);

        $repoMock = m::mock(DocumentManagementService::class);
        $repoMock->shouldReceive('getEcodedContentsFromUrl')->andReturn(base64_encode('filedata'));
        $repoMock->shouldReceive('getContents')
            ->andReturn(UploadedFile::fake()->image('img1.jpg')->openFile());

        $this->app->instance(DocumentManagementService::class, $repoMock);
        $this->app->instance('MongoClient', $mockMongoClient);
    }

    /**
     *
     */
    public function testShouldGetInfoFromWebImageUrl()
    {
        // set
        $httpClientMock = m::mock(\GuzzleHttp\Client::class);
        $responseMock = m::mock(\GuzzleHttp\Psr7\Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn($this->responseSafe);

        $httpClientMock->shouldReceive('post')
            ->once()
            ->andReturn($responseMock);

        $this->app->instance(\GuzzleHttp\Client::class, $httpClientMock);

        // act
        $service = new GoogleVisionClient();
        $imageUrl = 'http://exemplo.com.br/fotos/foto1.jpg';
        $info = $service->analyze($imageUrl);

        // assert
        $this->assertNotEmpty($info);
    }


    /**
     *
     */
    public function testShouldReturnFalseWhenImageHasNoExplicitContent()
    {
        // set
        $httpClientMock = m::mock(\GuzzleHttp\Client::class);
        $responseMock = m::mock(\GuzzleHttp\Psr7\Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn($this->responseSafe);

        $httpClientMock->shouldReceive('post')
            ->once()
            ->andReturn($responseMock);

        $this->app->instance(\GuzzleHttp\Client::class, $httpClientMock);

        // act
        $service = new GoogleVisionClient();
        $imageUrl = 'http://exemplo.com.br/fotos/conteudoSeguro.jpg';

        $result = $service->analyze($imageUrl);

        // assert
        $this->assertFalse($result->hasExplicitContent());
    }


    /**
     *
     */
    public function testShouldDetectAdultContent()
    {
        // set
        $httpClientMock = m::mock(\GuzzleHttp\Client::class);
        $responseMock = m::mock(\GuzzleHttp\Psr7\Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn($this->responseUnsafe);

        $httpClientMock->shouldReceive('post')
            ->once()
            ->andReturn($responseMock);

        $this->app->instance(\GuzzleHttp\Client::class, $httpClientMock);

        // act
        $service = new GoogleVisionClient();
        $imageUrl = 'http://exemplo.com.br/fotos/conteudoExplicito.jpg';

        $result = $service->analyze($imageUrl);

        // assert
        $this->assertTrue($result->hasExplicitContent());
    }
}
