<?php

use App\Services\MSComputerVisionClient;
use Mockery as m;

class MicrosoftComputerVisionServiceTest extends TestCase
{
    protected $responseSafe = '{
                                 "adult": {
                                   "isAdultContent": false,
                                   "isRacyContent": false,
                                   "adultScore": 0.0225948728621006,
                                   "racyScore": 0.016370180994272232
                                 }
                               }';

    protected $responseUnsafe = '{
                                 "adult": {
                                   "isAdultContent": true,
                                   "isRacyContent": true,
                                   "adultScore": 0.9225948728621006,
                                   "racyScore": 0.916370180994272232
                                 }
                               }';
    /**
     *
     */
    public function testShouldGetInfoFromWebImageUrl()
    {
        // set
        $httpClientMock = m::mock(\GuzzleHttp\Client::class);
        $responseMock = m::mock(\GuzzleHttp\Psr7\Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn($this->responseSafe);

        $httpClientMock->shouldReceive('post')
            ->once()
            ->andReturn($responseMock);

        $this->app->instance(\GuzzleHttp\Client::class, $httpClientMock);

        // act
        $service = new MSComputerVisionClient();
        $imageUrl = 'http://exemplo.com.br/fotos/foto1.jpg';
        $info = $service->analyze($imageUrl);

        // assert
        $this->assertNotEmpty($info);
    }


    /**
     *
     */
    public function testShouldReturnFalseWhenImageHasNoExplicitContent()
    {
        // set
        $httpClientMock = m::mock(\GuzzleHttp\Client::class);
        $responseMock = m::mock(\GuzzleHttp\Psr7\Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn($this->responseSafe);

        $httpClientMock->shouldReceive('post')
            ->once()
            ->andReturn($responseMock);

        $this->app->instance(\GuzzleHttp\Client::class, $httpClientMock);

        // act
        $service = new MSComputerVisionClient();
        $imageUrl = 'http://exemplo.com.br/fotos/conteudoSeguro.jpg';

        $result = $service->analyze($imageUrl);

        // assert
        $this->assertFalse($result->hasExplicitContent());
    }

    /**
     *
     */
    public function testShouldDetectAdultContent()
    {
        // set
        $httpClientMock = m::mock(\GuzzleHttp\Client::class);
        $responseMock = m::mock(\GuzzleHttp\Psr7\Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn($this->responseUnsafe);

        $httpClientMock->shouldReceive('post')
            ->once()
            ->andReturn($responseMock);

        $this->app->instance(\GuzzleHttp\Client::class, $httpClientMock);

        // act
        $service = new MSComputerVisionClient();
        $imageUrl = 'http://exemplo.com.br/fotos/conteudoExplicito.jpg';

        $result = $service->analyze($imageUrl);

        // assert
        $this->assertTrue($result->hasExplicitContent());
    }


}
