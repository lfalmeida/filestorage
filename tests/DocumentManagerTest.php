<?php

use App\Contracts\DocumentManagementServiceInterface;
use App\Services\DocumentManagementService;
use App\Models\Document;
use Mockery as m;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileManagerTest
 */
class DocumenManagerTest extends TestCase
{
    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        Storage::put('tests/1493637470.5907195e503557.59064718.md', '#TestMD');
    }

    public function tearDown()
    {
        parent::tearDown();
        $path = __DIR__ .'/../storage/app/tests/*';
        $files = glob($path);
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    /**
     *
     */
    public function testShouldGetDocumentInfoBasedOnRelativeFilePath()
    {
        $doc = $this->getDocumentTestInfo();
        // verifica se é uma instância do tipo Document
        $this->assertInstanceOf(Document::class, $doc);
        // verifica se as propriedades esperadas foram recebidas
        $this->assertNotEmpty($doc->getId());
        $this->assertNotEmpty($doc->getOriginalName());
        $this->assertNotEmpty($doc->getExtension());
        $this->assertNotEmpty($doc->getMimeType());
        $this->assertNotEmpty($doc->getPath());
        $this->assertNotEmpty($doc->getStorageDisk());
        $this->assertNotEmpty($doc->getHash());
    }

    /**
     * Testa se é possível obter uma instância de Document com o
     * conteúdo obtido do disco
     */
    public function testShouldGetDocumentWithContentsFromDisk()
    {
        $docManager = $this->getFilemanagerWithFindMethodStubbed();
        $doc = $this->getDocumentTestInfo();
        $document = $docManager->get($doc->getPath());
        $document->setContents('Test document');
        $this->assertTrue(!empty($document->getContents()));
    }

    /**
     * Testa se é possível obter uma instância de Document com o
     * conteúdo obtido do disco
     */
    public function testShouldThrowExceptionWhenTryingToReadAnNonExistingFile()
    {
        // Obtendo um mock do repositório de documentos
        $docRepo = m::mock(\App\Contracts\DocumentRepositoryInterface::class);

        // Stub do método find que deve retornar um Document
        $docRepo->shouldReceive('find')
            ->once()
            ->andReturn(null);

        // Criação da instância do FileManager, passando o repositório mockado
        $docManager = new DocumentManagementService($docRepo);

        $this->expectException(\Exception::class);
        $docManager->get('invalidTest/path');
    }

    /**
     * Método auxiliar que retorna uma instância Document com dados para os testes
     * @return Document
     * @throws \App\Exceptions\DocumentNotFoundException
     */
    private function getDocumentTestInfo(): Document
    {
        $docManager = $this->getFilemanagerWithFindMethodStubbed();
        // Caminho relativo do arquivo que desejamos obter informações
        $relativeFilePath = 'tests/1493637470.5907195e503557.59064718.md';
        return $docManager->get($relativeFilePath);
    }

    /**
     * @return DocumentManagementServiceInterface
     * @throws Exception
     */
    private function getFilemanagerWithFindMethodStubbed(): DocumentManagementServiceInterface
    {
        // Obtendo um mock do repositório de documentos
        $docRepo = m::mock(\App\Contracts\DocumentRepositoryInterface::class);

        // Stub do método find que deve retornar um Document
        $docRepo->shouldReceive('find')
            ->andReturn(new Document([
                    "id" => "1493637470.5907195e503557.59064718",
                    "originalName" => "readme.md",
                    "extension" => "md",
                    "mimeType" => "text/markdown",
                    "path" => "tests/1493637470.5907195e503557.59064718.md",
                    "storageDisk" => "local",
                    "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
                ]
            ));

        // Criação da instância do FileManager, passando o repositório mockado
        return new DocumentManagementService($docRepo);
    }

    /**
     *
     * @throws Exception
     */
    public function testShouldDeleteDocument()
    {
        // Obtendo um mock do repositório de documentos
        $docRepo = m::mock(\App\Contracts\DocumentRepositoryInterface::class);

        $document = new Document([
                "id" => "1493637470.5907195e503557.59064718",
                "originalName" => "readme.md",
                "extension" => "md",
                "mimeType" => "text/markdown",
                "path" => "tests/1493637470.5907195e503557.59064718.md",
                "storageDisk" => "local",
                "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
            ]
        );

        // Stub do método find que deve retornar um Document
        $docRepo->shouldReceive('find')
            ->once()
            ->andReturn($document)
            ->shouldReceive('delete')
            ->once()
            ->andReturn(true);

        // Criação da instância do FileManager, passando o repositório mockado
        $docManager = new DocumentManagementService($docRepo);

        $result = $docManager->delete('tests/1493637470.5907195e503557.59064718.md');
        $this->assertTrue($result);
    }

    public function testShouldGetAUrlDocument()
    {
        $docManager = $this->getFilemanagerWithFindMethodStubbed();
        $doc = $this->getDocumentTestInfo();

        $document = $docManager->get($doc->getPath());

        $url = $docManager->getUrl($document);
        $this->assertNotEmpty($url);
    }

    public function testShouldGetAUrlDocumentFromS3()
    {
        $docManager = $this->getFilemanagerWithFindMethodStubbed();
        $doc = $this->getDocumentTestInfo();

        $document = $docManager->get($doc->getPath());
        $document->setStorageDisk('s3');

        $url = $docManager->getUrl($document);
        $this->assertNotEmpty($url);
    }

    /**
     *
     */
    public function testShouldThrowExceptionWhenInvalidStorageDiskIsProvidedWhenTryingToRetrieveURL()
    {
        $docManager = $this->getFilemanagerWithFindMethodStubbed();
        $doc = $this->getDocumentTestInfo();

        $document = $docManager->get($doc->getPath());
        $document->setStorageDisk('invalidProvider');

        $this->expectException(Exception::class);
        $docManager->getUrl($document);
    }


/**
 * TODO criar teste para obter base64 a partir de uma url
 */

}
