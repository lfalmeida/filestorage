<?php


namespace App\Http\Controllers {

    use App\Contracts\DocumentRepositoryInterface;
    use App\Contracts\ImageAnalysisServiceInterface;
    use App\Exceptions\DocumentNotFoundException;
    use App\Exceptions\MimeTypeNotAllowedException;
    use App\Exceptions\MimeTypeNotWhitelistedException;
    use App\Models\Document;
    use App\Repositories\DocumentRepositoryRedis;
    use App\Services\DocumentManagementService;
    use App\Services\MSComputerVisionClient;
    use Illuminate\Http\JsonResponse;
    use Illuminate\Http\Request;
    use Illuminate\Http\UploadedFile;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Storage;
    use Mockery as m;
    use TestCase;

    /**
     * Stub
     * sobrescreve a função nativa do php para este namespace
     * @param $path
     * @return array|bool
     */
    function exif_read_data($path)
    {
        switch ($path) {
            case '/validPath/image':
                $result = ['valid', 'array', 'returned'];
                break;
            case '/invalidPath/invalidFile.csv':
            case '':
            default:
                $result = false;
                break;
        }
        return $result;
    }


    class DocumentsControllerTest extends TestCase
    {

        public function setUp()
        {
            parent::setUp();
            $this->app->bind(
                DocumentRepositoryInterface::class,
                DocumentRepositoryRedis::class
            );
        }

        public function tearDown()
        {
            parent::tearDown();
            $path = __DIR__ . '/../storage/app/tests/*';
            $files = glob($path);
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }

        /**
         *
         */
        public function testShouldUploadMultipleFilesAndReturnFilesInfo()
        {
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $docRepo->shouldReceive('save')->andReturn(true);

            $repoMock = m::mock(DocumentManagementService::class);
            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            if (env('ENABLE_IMAGE_ANALYSIS')) {
                $imgAnalysis = m::mock(MSComputerVisionClient::class);
                $imgAnalysis->shouldReceive('analyze')->andReturn($imgAnalysis)
                    ->shouldReceive('hasExplicitContent')
                    ->andReturn(false);
                $this->app->instance(ImageAnalysisServiceInterface::class, $imgAnalysis);
            }

            $controller = new DocumentsController();
            $request = m::mock(Request::class);

            $fileUpload = UploadedFile::fake()->image('img1.jpg');

            $request->shouldReceive('file')
                ->once()
                ->andReturn([$fileUpload, $fileUpload]);

            $response = $controller->upload('tests', $request);

            /**
             * Deve retornar um código Http 200 (success)
             */
            $this->assertEquals(200, $response->getStatusCode());

            /**
             * Deve retornar uma instância da Classe JsonResponse
             */
            $this->assertInstanceOf(JsonResponse::class, $response);

            /**
             * Os dados recebidos na resposta não podem ser vazios
             */
            $dataReceived = $response->getData();
            $this->assertNotEmpty($dataReceived);

            /**
             * O objeto precisa conter uma propriedade 'data'
             */
            $this->assertTrue(property_exists($dataReceived, 'data'));

            /**
             * O propriedade 'data' não pode ser vazia
             */
            $this->assertNotEmpty($dataReceived->data);
            /**
             * Ao enviar múltiplos arquivos a propriedade 'data' da resposta é um array
             */
            $this->assertTrue(is_array($dataReceived->data));

            /**
             * Como foi enviado mais de um arquivo no upload, a resposta precisa conter
             * dados dos vários arquivos enviados
             */
            $this->assertTrue(count($dataReceived->data) > 1);
        }

        /**
         * @return mixed
         */
        public function testShouldReturnFileMetadata()
        {
            // set
            $response = $this->testShouldUploadOneFileAndReturnFileInfo();
            $responseData = ($response->getData())->data;

            $document = m::mock(Document::class);
            $document->shouldReceive('getId')
                ->andReturn($responseData->id)
                ->shouldReceive('getOriginalName')
                ->andReturn($responseData->originalName)
                ->shouldReceive('getExtension')
                ->andReturn($responseData->extension)
                ->shouldReceive('getMimeType')
                ->andReturn($responseData->mimeType)
                ->shouldReceive('getPath')
                ->andReturn($responseData->path)
                ->shouldReceive('getSize')
                ->andReturn($responseData->size)
                ->shouldReceive('getUrl')
                ->andReturn($responseData->url)
                ->shouldReceive('getStorageDisk')
                ->andReturn($responseData->storageDisk)
                ->shouldReceive('getHash')
                ->andReturn($responseData->hash)
                ->shouldReceive('getDateTime')
                ->andReturn($responseData->dateTime)
                ->shouldReceive('getExplicitContent')
                ->andReturn(true)
                ->shouldReceive('getExif')
                ->andReturn(null);
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $docService = m::mock(DocumentManagementService::class);
            $docService->shouldReceive('get')->andReturn($document);
            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $docService);
            $fileName = sprintf('%s.%s', $responseData->id, $responseData->extension);

            // act
            $controller = new DocumentsController();
            $response = $controller->showMetadata('tests', $fileName);
            $data = ($response->getData())->data;

            // assert
            $this->assertEquals(200, $response->getStatusCode());
            $this->assertNotEmpty($data);
            return $data;
        }

        /**
         *
         */
        public function testShouldUploadOneFileAndReturnFileInfo()
        {

            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $docRepo->shouldReceive('save')->andReturn(true);

            $repoMock = m::mock(DocumentManagementService::class);
            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            if (env('ENABLE_IMAGE_ANALYSIS')) {
                $imgAnalysis = m::mock(MSComputerVisionClient::class);
                $imgAnalysis->shouldReceive('analyze')->andReturn($imgAnalysis)
                    ->shouldReceive('hasExplicitContent')
                    ->andReturn(false);
                $this->app->instance(ImageAnalysisServiceInterface::class, $imgAnalysis);
            }

            $controller = new DocumentsController();
            $request = m::mock(Request::class);

            $fileUpload = UploadedFile::fake()->image('img1.jpg');

            $request->shouldReceive('file')
                ->once()
                ->andReturn($fileUpload);

            $response = $controller->upload('tests', $request);


            /**
             * Deve retornar um código Http 200 (success)
             */
            $this->assertEquals(200, $response->getStatusCode());

            /**
             * Deve retornar uma instância da Classe JsonResponse
             */
            $this->assertInstanceOf(JsonResponse::class, $response);

            /**
             * Os dados recebidos na resposta não podem ser vazios
             */
            $dataReceived = $response->getData();
            $this->assertNotEmpty($dataReceived);

            /**
             * O objeto precisa conter uma propriedade 'data'
             */
            $this->assertTrue(property_exists($dataReceived, 'data'));

            /**
             * O propriedade 'data' não pode ser vazia
             */
            $this->assertNotEmpty($dataReceived->data);

            /**
             * A classe precisa retornar a url onde o arquivo estará disponível
             */
            $this->assertNotEmpty($dataReceived->data->url);

            return $response;
        }

        /**
         *
         */
        public function testShouldThowExceptionWhenFileNotWhitelistedIsProvided()
        {
            $controller = new DocumentsController();
            $request = m::mock(Request::class);

            $this->app->instance('RedisClient', m::mock(\Predis\Client::class));
            $redisRepo = m::mock(DocumentRepositoryRedis::class);
            $this->app->instance('DocumentRepositoryInterface', $redisRepo);

            $file = UploadedFile::fake()->create('test.zip', 1200);

            $request->shouldReceive('file')
                ->andReturn($file);

            $this->expectException(MimeTypeNotWhitelistedException::class);
            $controller->upload('tests', $request);
        }

        /**
         *
         */
        public function testShouldThowExceptionWhenInvalidUploadedFileProvided()
        {
            $controller = new DocumentsController();
            $redisRepo = m::mock(DocumentRepositoryInterface::class);
            $redisRepo->shouldReceive('set');
            $this->app->instance('RedisClient', $redisRepo);

            $this->app->instance('DocumentRepositoryInterface', $redisRepo);

            $file = UploadedFile::fake()->create('test.pdf', 1200);
            $request = m::mock(Request::class);
            $request->shouldReceive('file')->andReturn($file);

            $this->expectException(MimeTypeNotAllowedException::class);
            $controller->upload('avatars', $request);
        }


        /**
         *
         */
        public function testShouldThrowExceptionWhenTryToHandleFileNotUploaded()
        {
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $repoMock = m::mock(DocumentManagementService::class);

            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            $controller = new DocumentsController();
            $request = m::mock(Request::class);

            $request->shouldReceive('file')
                ->once()
                ->andReturn(null);

            $this->expectException(\Exception::class);
            $controller->upload('tests', $request);

        }

        /**
         *
         */
        public function testShouldReturn404WhenTryToAccessNonExistingFile()
        {
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $repoMock = m::mock(DocumentManagementService::class);
            $repoMock->shouldReceive('get')
                ->once()
                ->andReturn(new Document([
                    'storageDisk' => 'local',
                    'mimeType' => 'text/markdown',
                    'originalName' => 'readme.md'
                ]));

            $repoMock->shouldReceive('getContents')->once()->andReturn(null);

            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            /**
             * Obtém uma instancia do controller que será testado e
             * uma instância mockada da classe Request
             */
            $controller = App::make(DocumentsController::class);
            $request = m::mock(Request::class);

            $this->expectException(DocumentNotFoundException::class);
            $response = $controller->showDocument(new Request(), 'tests', $request);

            /**
             * Deve retornar um código Http 404 (not found)
             */
            $this->assertEquals(404, $response->getStatusCode());
        }

        /**
         * Faz um fake upload de um arquivo e verifica se é possível exibir este arquivo
         */
        public function testShouldDisplayAnExistingFile()
        {
            // 1 - set
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            // Ao fazer o upload, o controller vai precisar invocar o método save do resitório e receber true
            $docRepo->shouldReceive('save')->andReturn(true);
            if (env('ENABLE_IMAGE_ANALYSIS')) {
                $imgAnalysis = m::mock(MSComputerVisionClient::class);
                $imgAnalysis->shouldReceive('analyze')->andReturn($imgAnalysis)
                    ->shouldReceive('hasExplicitContent')
                    ->andReturn(false);
                $this->app->instance(ImageAnalysisServiceInterface::class, $imgAnalysis);
            }
            //
            $repoMock = m::mock(DocumentManagementService::class);
            $repoMock->shouldReceive('getContents')
                ->andReturn(UploadedFile::fake()->image('img1.jpg')->openFile());

            // preperando o documento que será retornando pelo mock do repositório
            $documentMock = m::mock(Document::class);
            $documentMock->shouldReceive('getStorageDisk')
                ->andReturn('local')
                ->shouldReceive('getContents')
                ->andReturn(UploadedFile::fake()->image('img1.jpg')->openFile())
                ->shouldReceive('getMimeType')->andReturn('image/jpeg')
                ->shouldReceive('getOriginalName')->andReturn('img1.jpg');

            $repoMock->shouldReceive('get')->andReturn($documentMock);

            // fazendo bind para que as instâncias mockadas seja usadas pelo framework ao executar o teste
            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            $controller = new DocumentsController();
            // preparando o mock da request
            $request = m::mock(Request::class);

            $request->shouldReceive('except')
                ->andReturn([]);

            // Mock de uma instância UploadedFile
            $fileUpload = UploadedFile::fake()->image('img1.jpg');

            // Quando este request mockado receber uma chamada para o método file, irá retornar
            // a instância que foi criada acima
            $request->shouldReceive('file')
                ->once()
                ->andReturn($fileUpload);

            // act
            $response = $controller->upload('tests', $request);
            $data = ($response->getData())->data;
            $response = $controller->showDocument($request, 'tests', "$data->id.$data->extension");

            // assert
            // precisa retornar um status 200 - OK
            $this->assertEquals(200, $response->getStatusCode());

        }

        /**
         *
         */
        public function testShouldDisplayFileFromExternalDisk()
        {
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $repoMock = m::mock(DocumentManagementService::class);
            $repoMock->shouldReceive('get')
                ->once()
                ->andReturn(new Document([
                    'storageDisk' => 's3',
                    'mimeType' => 'text/markdown',
                    'originalName' => 'readme.md'
                ]));

            $repoMock->shouldReceive('getUrl')->once()->andReturn('http://placehold.it/100x100');

            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            /**
             * Obtém uma instancia do controller que será testado e
             * uma instância mockada da classe Request
             */
            $controller = new DocumentsController();
            $response = $controller->showDocument(new Request(), 'tests', 'readme.md');

            /**
             * Deve retornar um código Http 302 (redirect)
             */
            $this->assertEquals(302, $response->getStatusCode());
        }

        /**
         *
         */
        public function testShouldDeleteAFile()
        {
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $repoMock = m::mock(DocumentManagementService::class);
            $repoMock->shouldReceive('delete')->once()->andReturn(true);

            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            /**
             * Obtém uma instancia do controller que será testado e
             * uma instância mockada da classe Request
             */
            $controller = new DocumentsController();
            $controller->deleteFile('tests', '1493637470.5907195e503557.59064718.md');

            //
            Storage::put('tests/1493637470.5907195e503557.59064718.md', '#TestMD');
        }

        /**
         *
         */
        public function testShouldReturnThrowExceptionWhenFailsToDeleteAFile()
        {
            $docRepo = m::mock(DocumentRepositoryInterface::class);
            $repoMock = m::mock(DocumentManagementService::class);
            $repoMock->shouldReceive('delete')->once()->andReturn(false);

            $this->app->instance(DocumentRepositoryInterface::class, $docRepo);
            $this->app->instance(DocumentManagementService::class, $repoMock);

            /**
             * Obtém uma instancia do controller que será testado e
             * uma instância mockada da classe Request
             */
            $controller = new DocumentsController();

            $this->expectException(\Exception::class);
            $controller->deleteFile('tests', '1493637470.5907195e503557.59064718.md');

        }


        /**
         *
         */
        public function testShouldManipulateImageWhenRequestParametersProvided()
        {
            // set
            $response = $this->testShouldUploadOneFileAndReturnFileInfo();
            $responseData = ($response->getData())->data;

            $docService = m::mock(DocumentRepositoryInterface::class);
            $repoSerice = m::mock(DocumentManagementService::class);

            $document = m::mock(Document::class);
            $document->shouldReceive('getId')
                ->andReturn($responseData->id)
                ->shouldReceive('getMimeType')
                ->andReturn($responseData->mimeType)
                ->shouldReceive('getPath')
                ->andReturn($responseData->path)
                ->shouldReceive('getStorageDisk')
                ->andReturn($responseData->storageDisk);

            $repoSerice->shouldReceive('get')
                ->andReturn($document);

            $glideServerFactory = m::mock('alias:\League\Glide\ServerFactory');

            $glideServer = m::mock(\League\Glide\Server::class);
            $glideServer->shouldReceive('outputImage')
                ->andReturn(true);

            $glideServerFactory->shouldReceive('create')
                ->andReturn($glideServer);

            $this->app->bind(\League\Glide\ServerFactory::class, $glideServerFactory);

            $this->app->instance(DocumentRepositoryInterface::class, $docService);
            $this->app->instance(DocumentManagementService::class, $repoSerice);

            $request = m::mock(Request::class);
            $request->shouldReceive('except')
                ->andReturn(['w' => 300])
                ->shouldReceive('all')
                ->andReturn(['w' => 300]);

            $controller = new DocumentsController();
            list($resource, $file) = explode('/', $responseData->path);

            $response = $controller->showDocument($request, $resource, $file);
            return $response;

        }

        /**
         *
         */
//        public function testShouldUploadAValidBase64File()
//        {
//            $img = "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACklEQVR4nGP6DwABBQECz6AuzQAAAABJRU5ErkJggg==";
//            $controller = new DocumentsController();
//            $request = m::mock(Request::class);
//
//            $request->shouldReceive('file')
//                ->andReturn(null)
//                ->shouldReceive('except')
//                ->andReturn(['img' => $img]);
//
//            if (env('ENABLE_IMAGE_ANALYSIS')) {
//                $imgAnalysis = m::mock(MSComputerVisionClient::class);
//                $imgAnalysis->shouldReceive('analyze')->andReturn($imgAnalysis)
//                    ->shouldReceive('hasExplicitContent')
//                    ->andReturn(false);
//                $this->app->instance(ImageAnalysisServiceInterface::class, $imgAnalysis);
//            }
//
//            $client = m::mock(\Predis\Client::class);
//            $client->shouldReceive('set')->andReturn(1);
//
//            $this->app->instance('RedisClient', $client);
//            $redisRepo = m::mock(DocumentRepositoryRedis::class);
//
//            $this->app->instance('DocumentRepositoryInterface', $redisRepo);
//
//            $response = $controller->upload('avatars', $request);
//
//            $this->assertNotEmpty($response);
//            $this->assertEquals(200, $response->getStatusCode());
//        }

        /**
         *
         */
        public function testShouldThrowExceptionWhenInvalidBase64DataProvided()
        {
            // base64 inválido
            $img = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACklEQVR4nGP6DwABBQECz6AuzQAAAABJRU5ErkJggg==";
            $controller = new DocumentsController();
            $request = m::mock(Request::class);
            $request->shouldReceive('file')
                ->andReturn(null)
                ->shouldReceive('except')
                ->andReturn(['img' => $img]);

            $client = m::mock(\Predis\Client::class);
            $client->shouldReceive('set')->andReturn(1);

            $this->app->instance('RedisClient', $client);
            $redisRepo = m::mock(DocumentRepositoryRedis::class);

            $this->app->instance('DocumentRepositoryInterface', $redisRepo);

            $this->expectException(\Exception::class);
            $controller->upload('tests', $request);

        }

        /**
         *
         */
        public function testShouldReturnFalseWhenDataProvidedIsNotBase64Encoded()
        {
            //
            $invalidFile = "invalidString";
            $controller = new DocumentsController();
            $request = m::mock(Request::class);
            $request->shouldReceive('file')
                ->andReturn(null)
                ->shouldReceive('except')
                ->andReturn(['file' => $invalidFile]);

            $client = m::mock(\Predis\Client::class);
            $client->shouldReceive('set')->andReturn(1);

            $this->app->instance('RedisClient', $client);
            $redisRepo = m::mock(DocumentRepositoryRedis::class);

            $this->app->instance('DocumentRepositoryInterface', $redisRepo);

            $this->expectException(\Exception::class);
            $controller->upload('tests', $request);

        }

    }
}
