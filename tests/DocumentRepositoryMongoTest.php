<?php
use App\Repositories\DocumentRepositoryMongo;
use Mockery as m;

class DocumentRepositoryMongoTest extends TestCase
{
    protected $repo;

    public function setUp()
    {
        parent::setUp();
        $mockMongoClient = m::mock(MongoDB\Client::class);
        $mockMongoClient->db = (object)['db'];
        $mockMongoClient->db->docs = (object)['docs'];
        $this->app->instance('MongoClient', $mockMongoClient);
        $this->repo = new DocumentRepositoryMongo();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     *
     */
    public function testShouldSaveADocument()
    {
        // 1 - arrange
        $mockMongoClient = m::mock(MongoDB\Client::class);
        $mockMongoClient->db = (object)['db'];
        $mockMongoClient->db->docs = m::mock(MongoDB\Collection::class);
        $mockResult = m::mock(MongoDB\InsertOneResult::class);
        $mockBsonObjResult = m::mock(MongoDB\BSON\ObjectID::class);
        $mockBsonObjResult->oid = 'fasdfa09444kasdf9';
        $mockResult->shouldReceive('getInsertedId')
            ->andReturn($mockBsonObjResult);
        $mockMongoClient->db->docs->shouldReceive('insertOne')
            ->andReturn($mockResult);
        $this->app->instance('MongoClient', $mockMongoClient);

        // act
        $repo = new DocumentRepositoryMongo();
        $result = $repo->save([
            "id" => "1493637470.5907195e503557.59064718",
            "originalName" => "readme.md",
            "extension" => "md",
            "mimeType" => "text/markdown",
            "path" => "tests/1493637470.5907195e503557.59064718.md",
            "storageDisk" => "local",
            "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
        ]);

        // assert
        $this->assertTrue(boolval($result));
    }


    /**
     *
     */
    public function testShouldFindAExistingDocument()
    {
//        // arrange
        $mockMongoClient = m::mock(MongoDB\Client::class);
        $mockMongoClient->db = (object)['db'];
        $mockMongoClient->db->docs = m::mock(MongoDB\Collection::class);

        $mockResult = m::mock(stdClass::class);
        $mockResult->shouldReceive('getArrayCopy')
            ->andReturn([
                "id" => "1493637470.5907195e503557.59064718",
                "originalName" => "readme.md",
                "extension" => "md",
                "mimeType" => "text/markdown",
                "path" => "tests/1493637470.5907195e503557.59064718.md",
                "storageDisk" => "local",
                "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
            ]);

        $mockMongoClient->db->docs->shouldReceive('findOne')
            ->andReturn($mockResult);

        $this->app->instance('MongoClient', $mockMongoClient);

        // act
        $repo = new DocumentRepositoryMongo();
        $docResult = $repo->find(['path' => 'tests/1493637470.5907195e503557.59064718.md']);

        $this->assertInstanceOf(\App\Models\Document::class, $docResult);
    }

    /**
     *
     */
    public function testShouldThrowExceptionWhenTryingToFindANonExistingDocument()
    {
        // arrange
        $mockMongoClient = m::mock(MongoDB\Client::class);
        $mockMongoClient->db = (object)['db'];
        $mockMongoClient->db->docs = m::mock(MongoDB\Collection::class);
        $mockResult = [];
        $mockMongoClient->db->docs->shouldReceive('findOne')
            ->andReturn($mockResult);
        $this->app->instance('MongoClient', $mockMongoClient);

        // assert
        $this->expectException(Exception::class);

        // act
        $repo = new DocumentRepositoryMongo();
        $repo->find(['id' => 'fasdf93453498']);
    }

    /**
     *
     */
    public function testShouldDeleteAExistingDocument()
    {
        // arrange
        $mockMongoClient = m::mock(MongoDB\Client::class);
        $mockMongoClient->db = (object)['db'];
        $mockMongoClient->db->docs = m::mock(MongoDB\Collection::class);

        $mockMongoClient->db->docs->shouldReceive('deleteOne')
            ->andReturn(true);

        $this->app->instance('MongoClient', $mockMongoClient);

        // act
        $repo = new DocumentRepositoryMongo();
        $result = $repo->delete(['path' => 'tests/1493637470.5907195e503557.59064718.md']);

        $this->assertTrue($result);
    }
}
