<?php

class DocumentTest extends TestCase
{
    /**
     *
     */
    public function testShouldCanDefineAvailableProperties()
    {
        $doc = new \App\Models\Document([
            "id" => "1493637470.5907195e503557.59064718",
            "originalName" => "readme.md",
            "extension" => "md",
            "mimeType" => "text/markdown",
            "path" => "tests/1493637470.5907195e503557.59064718.md",
            "storageDisk" => "local",
            "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
        ]);
        $this->assertInstanceOf(\App\Models\Document::class, $doc);
    }

    /**
     *
     */
    public function testShouldNotCanDefineUnvailableProperties()
    {
        $this->expectException(Exception::class);
        new \App\Models\Document([
            "invalidProp" => "1493637470.5907195e503557.59064718",
        ]);
    }

    /**
     *
     */
    public function testShouldCanAccessAvailableProperties()
    {
        $doc = new \App\Models\Document([
            "id" => "1493637470.5907195e503557.59064718",
        ]);
        $this->assertTrue($doc->getId() === '1493637470.5907195e503557.59064718');
    }



}
