<?php

use App\Contracts\DocumentRepositoryInterface;
use App\Services\DocumentManagementService;
use Illuminate\Http\UploadedFile;
use Mockery as m;
use App\Contracts\ImageAnalysisServiceInterface;

class ImageAnalysisServiceTest extends TestCase
{
    protected $responseUnsafe = '{
                  "responses": [
                    {
                      "safeSearchAnnotation": {
                        "adult": "VERY_LIKELY",
                        "spoof": "LIKELY",
                        "medical": "UNLIKELY",
                        "violence": "UNLIKELY"
                      }
                    }
                  ]
                }';

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        $mockMongoClient = m::mock(MongoDB\Client::class);
        $collection = m::mock(MongoDB\Collection::class);

        $collection->shouldReceive('replaceOne')->andReturn(m::mock(MongoDB\UpdateResult::class));

        $db = (m::mock(MongoDB\Database::class))
            ->shouldReceive('selectCollection')
            ->andReturn($collection);
        $db->docs = $collection;

        $mockResult = m::mock(stdClass::class);
        $mockResult->shouldReceive('getArrayCopy')
            ->andReturn([
                "id" => "1493637470.5907195e503557.59064718",
                "originalName" => "readme.md",
                "extension" => "md",
                "mimeType" => "text/markdown",
                "path" => "tests/1493637470.5907195e503557.59064718.md",
                "storageDisk" => "local",
                "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
            ]);

        $mockMongoClient->shouldReceive('selectDatabase')->andReturn($db);
        $mockMongoClient->db->docs->shouldReceive('findOne')
            ->andReturn($mockResult);


        $repoMock = m::mock(DocumentManagementService::class);
        $repoMock->shouldReceive('getEcodedContentsFromUrl')->andReturn(base64_encode('filedata'));
        $repoMock->shouldReceive('getContents')
            ->andReturn(UploadedFile::fake()->image('img1.jpg')->openFile());

        $this->app->instance(DocumentManagementService::class, $repoMock);

        $this->app->instance('MongoClient', $mockMongoClient);
        $this->app->bind(
            DocumentRepositoryInterface::class,
            \App\Repositories\DocumentRepositoryMongo::class
        );
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     *
     */
    public function testShouldDetectAdultContentUsingAClient()
    {
        // set
        $httpClientMock = m::mock(\GuzzleHttp\Client::class);
        $responseMock = m::mock(\GuzzleHttp\Psr7\Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn($this->responseUnsafe);

        $httpClientMock->shouldReceive('post')
            ->once()
            ->andReturn($responseMock);

        $this->app->instance(\GuzzleHttp\Client::class, $httpClientMock);

        $document = new \App\Models\Document([
            "id" => "1493637470.5907195e503557.59064718",
            "originalName" => "fotinha.jpg",
            "extension" => "jpg",
            "mimeType" => "text/markdown",
            "path" => "tests/1493637470.5907195e503557.59064718.jpg",
            "storageDisk" => "local",
            "hash" => "89134f172b84fb6eda713f9e8f2bfd2a"
        ]);

        $service = app(ImageAnalysisServiceInterface::class);

        // act
        $result = $service->analyze($document)
            ->hasExplicitContent();

        // assert
        $this->assertNotEmpty($result);
        $this->assertTrue($result);
    }
}
