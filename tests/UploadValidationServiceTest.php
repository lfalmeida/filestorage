<?php

use App\Services\UploadValidationService;
use Mockery as m;

class UploadValidationServiceTest extends TestCase
{

    /**
     *
     */
    public function testShouldThrowExceptionWhenInvalidateMimeTypeIsProvided()
    {
        $uploadFile = m::mock(\Illuminate\Http\UploadedFile::class);
        $uploadFile->shouldReceive('getMimeType')
            ->andReturn('application/json');
        $validator = new UploadValidationService();
        $allowedMimeTypes = ['image/jpeg'];
        $this->expectException(\App\Exceptions\MimeTypeNotWhitelistedException::class);
        $validator->validateMimeType($uploadFile, $allowedMimeTypes);
    }

    /**
     *
     */
    public function testValidateImageShouldReturnTrueWhenValidateMimeTypeIsProvided()
    {
        $uploadFile = m::mock(\Illuminate\Http\UploadedFile::class);
        $uploadFile->shouldReceive('getMimeType')
            ->andReturn('image/jpeg');
        $validator = new UploadValidationService();
        $this->assertTrue($validator->validateImage($uploadFile));
    }
}
