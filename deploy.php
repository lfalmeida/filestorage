<?php

namespace Deployer;

require 'vendor/autoload.php';
require_once 'recipe/common.php';

// Configuration
set('default_stage', 'staging');
set('repository', 'git@bitbucket.org:ebercom/filestorage.git');
set('branch', 'master');
set('composer_options', 'install --no-interaction --no-progress --prefer-dist --ignore-platform-reqs');
set('ssh_multiplexing', true);
// Hosts
host('storage.premiototall.com.br')
    ->stage('staging')
    ->set('deploy_path', '/var/ENV_DOCKER/src/FileStorage')
    ->user('ec2-user')
    ->port(22)
    ->addSshOption('StrictHostKeyChecking', 'no');

// Laravel shared dirs
set('shared_dirs', [
    'storage',
]);

// Laravel shared file
set('shared_files', [
    '.env',
]);

// Laravel writable dirs
set('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);

set('laravel_version', function () {
    $result = run('{{bin/php}} {{release_path}}/artisan --version');

    preg_match_all('/(\d+\.?)+/', $result, $matches);

    $version = $matches[0][0] ?? 5.4;

    return $version;
});

/**
 * Helper tasks
 */
desc('Disable maintenance mode');
task('artisan:up', function () {
    $output = run('if [ -f {{deploy_path}}/current/artisan ]; then {{bin/php}} {{deploy_path}}/current/artisan up; fi');
    writeln('<info>' . $output . '</info>');
});

desc('Enable maintenance mode');
task('artisan:down', function () {
    $output = run('if [ -f {{deploy_path}}/current/artisan ]; then {{bin/php}} {{deploy_path}}/current/artisan down; fi');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan migrate');
task('artisan:migrate', function () {
    run('{{bin/php}} {{release_path}}/artisan migrate --force');
});

desc('Execute artisan migrate:rollback');
task('artisan:migrate:rollback', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan migrate:rollback --force');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan migrate:status');
task('artisan:migrate:status', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan migrate:status');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan db:seed');
task('artisan:db:seed', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan db:seed --force');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan route:cache');
task('artisan:route:cache', function () {
    run('{{bin/php}} {{release_path}}/artisan route:cache');
});

desc('Execute artisan queue:restart');
task('artisan:queue:restart', function () {
//    run('{{bin/php}} {{release_path}}/artisan queue:restart');
    run('docker exec -itd docker_ph_php_1 bash -c "php /src/FileStorage/current/artisan queue:restart"');
});

desc('Execute artisan storage:link');
task('artisan:storage:link', function () {
    $needsVersion = 5.3;
    $currentVersion = get('laravel_version');

    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/artisan storage:link');
    }
});

/**
 * Task deploy:public_disk support the public disk.
 * To run this task automatically, please add below line to your deploy.php file
 *
 *     before('deploy:symlink', 'deploy:public_disk');
 *
 * @see https://laravel.com/docs/5.2/filesystem#configuration
 */
desc('Make symlink for public disk');
task('deploy:public_disk', function () {
    // Remove from source.
    run('if [ -d $(echo {{release_path}}/public/storage) ]; then rm -rf {{release_path}}/public/storage; fi');

    // Create shared dir if it does not exist.
    run('mkdir -p {{deploy_path}}/shared/storage/app/public');

    // Symlink shared dir to release dir
    run('{{bin/symlink}} {{deploy_path}}/shared/storage/app/public {{release_path}}/public/storage');
});

desc('Faz um post no slack');
task('slackMessage:deploySuccess', function () {
    $releasePath = get('release_path');
    $release = get('release_name');
    $host = get('hostname');
    $stage = get('stage');
    $branch = '*Branch:* ' . get('branch');
    if (input()->hasOption('branch')) {
        $inputBranch = input()->getOption('branch');
        if (!empty($inputBranch)) {
            $branch = '*Branch:* ' . $inputBranch;
        }
    }

    $tag = '';
    if (input()->hasOption('tag')) {
        $inputTag = input()->getOption('tag');
        if (!empty($inputTag)) {
            $tag = '*Tag:* ' . $inputTag;
        }
    }
    $webhookUrl = getenv('SLACK_WEBHOOK');
    $client = new \Maknz\Slack\Client($webhookUrl, [
        'username' => "DeployBot",
        'icon' => 'http://storage.premiototall.com.br/favicon.png'
    ]);
    $client->send(":white_check_mark: *FileStorage* \n 
     Deploy realizado com sucesso no ambiente `$stage` \n
     *Release:* $release \n
     $branch \n
     $tag");
});


desc('Faz um post no slack');
task('slackMessage:deployFail', function () {
    $releasePath = get('release_path');
    $release = get('release_name');
    $host = get('hostname');
    $stage = get('stage');
    $branch = '*Branch:* ' . get('branch');
    if (input()->hasOption('branch')) {
        $inputBranch = input()->getOption('branch');
        if (!empty($inputBranch)) {
            $branch = '*Branch:* ' . $inputBranch;
        }
    }

    $tag = '';
    if (input()->hasOption('tag')) {
        $inputTag = input()->getOption('tag');
        if (!empty($inputTag)) {
            $tag = '*Tag:* ' . $inputTag;
        }
    }
    $webhookUrl = getenv('SLACK_WEBHOOK');
    $client = new \Maknz\Slack\Client($webhookUrl, [
        'username' => "DeployBot",
        'icon' => 'http://storage.premiototall.com.br/favicon.png'
    ]);
    $client->send(":warning: *FileStorage* \n 
      Problema no deploy para o ambiente `$stage` \n
     *Release:* $release \n
     $branch \n
     $tag");

});

/**
 * Main task
 */
desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'deploy:symlink',
    'artisan:queue:restart',
    'deploy:unlock',
    'cleanup',
]);

after('deploy', 'success');
after('deploy', 'slackMessage:deploySuccess');
after('deploy:failed', 'deploy:unlock');
after('deploy:failed', 'slackMessage:deployFail');

