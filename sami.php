<?php

require __DIR__ . '/vendor/autoload.php';

use Sami\Sami;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('Resources')
    ->exclude('Tests')
    ->in(__DIR__ . '/app');


return new Sami($iterator, array(
    'title' => 'FileStorage',
    'build_dir' => __DIR__ . '/public/_docs',
    'cache_dir' => __DIR__ . '/storage/sami',
));


